<?php
// $Id: kaltura.inc,v 1.1.2.1 2010/10/29 01:36:11 junedkazi Exp $

/**
 * @file
 *  This is an kaltura provider include file for Embedded Media Video.
 *  Author:  Bobby Kramer, 2011-04-13, based on emfield example
 *
 *  When using this, first make the following global replacements:
 *    * Replace kaltura with the name of your provider in all caps.
 *    * Replace kaltura with the name of your provider in all lower case.
 *    * Replace kaltura with the name (to be translated) of your provider in
 *        uppercase.
 *
 *  You then need to go through each function and modify according to the
 *  requirements of your provider's API.
 */

/**
 *  This is the main URL for your provider.
 */
define('EMVIDEO_KALTURA_MAIN_URL', 'http://www.kaltura.com/');

/**
 *  This defines the version of the content data array that we serialize
 *  in emvideo_kaltura_data(). If we change the expected keys of that array,
 *  we must increment this value, which will allow older content to be updated
 *  to the new version automatically.
 */
define('EMVIDEO_KALTURA_DATA_VERSION', 1);

/**
 * hook emvideo_PROVIDER_info
 * This returns information relevant to a specific 3rd party video provider.
 *
 * @return
 *   A keyed array of strings requested by various admin and other forms.
 *    'provider' => The machine name of the provider. This must be the same as
 *      the base name of this filename, before the .inc extension.
 *    'name' => The translated name of the provider.
 *    'url' => The url to the main page for the provider.
 *    'settings_description' => A description of the provider that will be
 *      posted in the admin settings form.
 *    'supported_features' => An array of rows describing the state of certain
 *      supported features by the provider. These will be rendered in a table,
 *      with the columns being 'Feature', 'Supported', 'Notes'. In general,
 *      the 'Feature' column will give the name of the feature, 'Supported'
 *      will be Yes or No, and 'Notes' will give an optional description or
 *      caveats to the feature.
 */
function emvideo_kaltura_info() {
  $features = array(
    array(t('Full screen mode'), t('Yes'), t('You may customize the player to enable or disable full screen playback. Full screen mode is enabled by default.')),
  );
  return array(
    'provider' => 'kaltura',
    'name' => t('Kaltura'),
    'url' => EMVIDEO_KALTURA_MAIN_URL,
    'settings_description' => t('These settings specifically affect videos displayed from !kaltura.', array('!kaltura' => l(t('Kaltura.com'), EMVIDEO_KALTURA_MAIN_URL))),
    'supported_features' => $features,
  );
}

/**
 *  hook emvideo_PROVIDER_settings
 *  This should return a subform to be added to the emvideo_settings() admin
 *  settings page.
 *
 *  Note that a form field set will already be provided at $form['kaltura'],
 *  so if you want specific provider settings within that field set, you should
 *  add the elements to that form array element.
 */
function emvideo_kaltura_settings() {
  // We'll add a field set of player options here. You may add other options
  // to this element, or remove the field set entirely if there are no
  // user-configurable options allowed by the kaltura provider.
  $form['kaltura']['player_options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Embedded video player options'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  // This is an option to set the video to full screen. You should remove this
  // option if it is not provided by the kaltura provider.
  $form['kaltura']['player_options']['emvideo_kaltura_full_screen'] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow fullscreen'),
    '#default_value' => variable_get('emvideo_kaltura_full_screen', 1),
    '#description' => t('Allow users to view video using the entire computer screen.'),
  );

  return $form;
}

/**
 *  hook emvideo_PROVIDER_extract
 *
 *  This is called to extract the video code from a pasted URL or embed code.
 *
 *  We'll be passed a URL or the embed code from a video when an editor pastes
 *  that in the field's textfield. We'll need to either pass back an array of
 *  regex expressions to match, or do the matching ourselves and return the
 *  resulting video code.
 *
 *  @param $parse
 *    An optional string with the pasted URL or embed code.
 *  @return
 *    Either an array of regex expressions to be tested, or a string with the
 *    video code to be used. If the hook tests the code itself, it should
 *    return either the string of the video code (if matched), or an empty
 *    array. Otherwise, the calling function will handle testing the embed code
 *    against each regex string in the returned array.
 */
function emvideo_kaltura_extract($parse = '') {
	return array(
    '@kaltura\.com/index.php/kwidget/([^"\?]+)@i',
	);
}


/**
 * The embedded flash displaying the kaltura video.
 */
function theme_emvideo_kaltura_flash($item, $width, $height, $autoplay) {
	$output = '';
	if ($item['embed']) {
		$tok = strtok($item['value'], " //");
		//split the url into pieces, so we can recreate the flash object below
		//I assume embedded-media module already sanitized the data..
		$url_pieces = array();
		//make use of string tokenizer, using / for the token
		while ($tok !== false) {
			$url_pieces[] = $tok;
			$tok = strtok("//");
		}
		//figure out autoplay or not		 
		$autoplay = $autoplay ? 'true' : 'false';
		//figure out fullscreen permissions for module
		$fullscreen = variable_get('emvideo_kaltura_full_screen', 1) ? 'true' : 'false';
		//apply the flash object for kaltura with several permissions
		$output =  '<object id="kaltura_player_' . $url_pieces[1] . '"
 		 name="kaltura_player" 
 		 type="application/x-shockwave-flash"
 		 height="' . $height . '" 
 		 width="' . $width . '" 
 		 xmlns:dc="http://purl.org/dc/terms/" 
 		 xmlns:media="http://search.yahoo.com/searchmonkey/media/" 
 		 rel="media:video" resource="http://www.kaltura.com/index.php/kwidget/cache_st/' .$url_pieces[1] .  '/wid/' . $url_pieces[3] . '/uiconf_id/' . $url_pieces[5] . '/entry_id/' . $url_pieces[7] . '" 
 		 data="http://www.kaltura.com/index.php/kwidget/wid/' . $url_pieces[3]  . '/uiconf_id/' . $url_pieces[5] . '/entry_id/' . $url_pieces[7] . '">';
		//add the rest of the flash params
		$output .= '<param name="allowFullScreen" value="'. $fullscreen .'" />';
		$output .= '<param name="allowNetworking" value="all" />';
		$output .= '<param name="allowScriptAccess" value="always" />';
 		$output .= '<param name="bgcolor" value="#000000" />';
 		$output .= '<param name="flashVars" value="&" />';
 		$output .= '<param name="FlashVars" value="&autoPlay='. $autoplay .'" />'; 		
 		$output .= '<param name="movie" value="http://www.kaltura.com/index.php/kwidget/' . $item['value'] .'">';
 		$output .= '<param name="quality" value="best" />';
 		$output .= '<param name="wmode" value="transparent" />';
  		$output .='</object>';
  }
  return $output;
}

/**
 * hook emvideo_PROVIDER_thumbnail
 * Returns the external url for a thumbnail of a specific video.
 *  @param $field
 *    The field of the requesting node.
 *  @param $item
 *    The actual content of the field from the requesting node.
 *  @return
 *    A URL pointing to the thumbnail.
 */
function emvideo_kaltura_thumbnail($field, $item, $formatter, $node, $width, $height) {
  // In this demonstration, we previously retrieved a thumbnail using oEmbed
  // during the data hook.
  return $data['thumbnail'];
}

/**
 *  hook emvideo_PROVIDER_video
 *  This actually displays the full/normal-sized video we want, usually on the
 *  default page view.
 *  @param $embed
 *    The video code for the video to embed.
 *  @param $width
 *    The width to display the video.
 *  @param $height
 *    The height to display the video.
 *  @param $field
 *    The field info from the requesting node.
 *  @param $item
 *    The actual content from the field.
 *  @return
 *    The html of the embedded video.
 */
function emvideo_kaltura_video($embed, $width, $height, $field, $item, $node, $autoplay) {
  $output = theme('emvideo_kaltura_flash', $item, $width, $height, $autoplay);
  return $output;
}

/**
 *  hook emvideo_PROVIDER_video
 *
 *  This actually displays the preview-sized video we want, commonly for the
 *  teaser.
 *  @param $embed
 *    The video code for the video to embed.
 *  @param $width
 *    The width to display the video.
 *  @param $height
 *    The height to display the video.
 *  @param $field
 *    The field info from the requesting node.
 *  @param $item
 *    The actual content from the field.
 *  @return
 *    The html of the embedded video.
 */
function emvideo_kaltura_preview($embed, $width, $height, $field, $item, &$node, $autoplay) {
  $output = theme('emvideo_kaltura_flash', $item, $width, $height, $autoplay);
  return $output;
}

/**
 *  Implementation of hook_emfield_subtheme.
 *  This returns any theme functions defined by this provider.
 */
function emvideo_kaltura_emfield_subtheme() {
    $themes = array(
        'emvideo_kaltura_flash'  => array(
            'arguments' => array('item' => NULL, 'width' => NULL, 'height' => NULL, 'autoplay' => NULL),
            'file' => 'providers/kaltura.inc',
            'path' => drupal_get_path('module', 'media_kaltura'),
        )
    );
    return $themes;
}
