Content Profile Feeds
=====================

Provides capability to create Feeds Importers that create/update
Content Profile node(s) when creating/updating users.

Note: only fields of type text are allowed.


Usage
=====

In order to enable Content Profile integration, your importer
have to use the "User & Content Profile Processor".

You then will be able to map parsed content to CCK fields on
Content Profile types.
