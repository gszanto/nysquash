<?php

/**
 * @file
 * ContentProfileFeedsUserProcessor class.
 */

/**
 * Feeds processor plugin. Create users from feed items.
 * Also, create/update content profile node(s).
 */
class ContentProfileFeedsUserProcessor extends FeedsProcessor {

  /**
   * Override parent::process().
   */
  public function process(FeedsImportBatch $batch, FeedsSource $source) {

    // Count number of created and updated nodes.
    $created  = $updated = $failed = 0;

    $content_profile_types = content_profile_get_types('names');
    $content_profile_stats = array();
    foreach ($content_profile_types as $profile_type => $profile_type_label) {
      $content_profile_stats[$profile_type]['created'] = 0;
      $content_profile_stats[$profile_type]['updated'] = 0;
    }

    while ($item = $batch->shiftItem()) {

      if (!($uid = $this->existingItemId($batch, $source)) || $this->config['update_existing']) {

        // If an existing user id id found...
        if (!empty($uid)) {

          // Load the user account and map it's values.
          $account = user_load($uid);
          $account = $this->map($batch, $account);
        }

        // Otherwise, just map it's values.
        else {
          $account = $this->map($batch);
        }

        // Store content profile values.
        $content_profile_feeds = $account->content_profile_feeds;

        // Automatically generate username from email, if configured to do so, and an existing username isn't already set.
        if ($this->config['auto_username'] && empty($account->name)) {

          /**
           * The following code is courtesy of Ubercart's uc_store_email_to_username() functions.
           */

          // Default to the first part of the e-mail address.
          $name = substr($account->mail, 0, strpos($account->mail, '@'));

          // Remove possible illegal characters.
          $name = preg_replace('/[^A-Za-z0-9_.-]/', '', $name);

          // Trim that value for spaces and length.
          $name = trim(substr($name, 0, USERNAME_MAX_LENGTH - 4));

          // Make sure we don't hand out a duplicate username.
          while (db_result(db_query("SELECT COUNT(uid) FROM {users} WHERE LOWER(name) = LOWER('%s')", $name)) > 0) {
            // If the username got too long, trim it back down.
            if (strlen($name) == USERNAME_MAX_LENGTH) {
              $name = substr($name, 0, USERNAME_MAX_LENGTH - 4);
            }

            // Append a random integer to the name.
            $name .= rand(0, 9);
          }

          // Assign it to the user.
          $account->name = $name;
        }

        // Check if user name and mail are set, otherwise continue.
        if (empty($account->name) || empty($account->mail) || !valid_email_address($account->mail)) {
          $failed++;
          continue;
        }

        // Save the user.
        user_save($account, (array) $account);

        // Map openid data, if provided.
        if ($account->uid && !empty($account->openid)) {
          $authmap = array(
            'uid' => $account->uid,
            'module' => 'openid',
            'authname' => $account->openid,
          );
          if (SAVED_UPDATED != drupal_write_record('authmap', $authmap, array('uid', 'module'))) {
            drupal_write_record('authmap', $authmap);
          }
        }

        // If a user id is available, increment the updated counter. Otherwise increment the created counter.
        if ($uid) {
          $updated++;
        }
        else {
          $created++;
        }

        // Create/update content profile node(s).
        if (!$account->uid) {
          $account = user_load(array('mail' => $account->mail));
        }

        foreach ($content_profile_feeds as $type_name => $fields) {
          if (array_key_exists($type_name, $content_profile_types) && !empty($fields)) {
            // Try to load existing node.
            $profile_node = content_profile_load($type_name, $account->uid);
            if ($profile_node) {
              $content_profile_stats[$profile_type]['updated']++;
            }
            else {
              $profile_node = array(
                'uid' => $account->uid,
                'name' => isset($account->name) ? $account->name : '',
                'type' => $type_name,
                'language' => '',
              );
              $profile_node = (object) $profile_node;

              $content_profile_stats[$profile_type]['created']++;
            }

            // Populate fields values.
            foreach ($fields as $field_name => $value) {
              $profile_node->{$field_name} = $value;
            }

            // "Commit" changes.
            node_save($profile_node);
          }
        }
      }
    }

    // Set messages.
    if ($failed) {
      drupal_set_message(
        format_plural(
          $failed,
          'There was @number user that could not be imported because either their name or their email was empty or not valid. Check import data and mapping settings on User processor.',
          'There were @number users that could not be imported because either their name or their email was empty or not valid. Check import data and mapping settings on User processor.',
          array('@number' => $failed)
        ),
        'error'
      );
    }
    if ($created) {
      drupal_set_message(format_plural($created, 'Created @number user.', 'Created @number users.', array('@number' => $created)));
    }
    elseif ($updated) {
      drupal_set_message(format_plural($updated, 'Updated @number user.', 'Updated @number users.', array('@number' => $updated)));
    }
    else {
      drupal_set_message(t('There are no new users.'));
    }

    foreach ($content_profile_types as $profile_type => $profile_type_label) {
      $params = array('@type' => $profile_type_label);
      if ($content_profile_stats[$profile_type]['created']) {
        $params['@number'] = $content_profile_stats[$profile_type]['created'];
        drupal_set_message(format_plural($content_profile_stats[$profile_type]['created'], 'Created @number @type node.', 'Created @number @type nodes.', $params));
      }
      if ($content_profile_stats[$profile_type]['updated']) {
        $params['@number'] = $content_profile_stats[$profile_type]['updated'];
        drupal_set_message(format_plural($content_profile_stats[$profile_type]['created'], 'Updated @number @type node.', 'Updated @number @type nodes.', $params));
      }
    }
  }

  /**
   * Implementation of FeedsProcessor::clear().
   */
  public function clear(FeedsBatch $batch, FeedsSource $source) {
    // Do not support deleting users as we have no way of knowing which ones we
    // imported.
    throw new Exception(t('User processor does not support deleting users.'));
  }

  /**
   * Execute mapping on an item.
   */
  protected function map(FeedsImportBatch $batch, $target_account = NULL) {
    // Prepare user account object.
    if (empty($target_account)) {
      $target_account = new stdClass();
      $target_account->uid = 0;
      $target_account->status = $this->config['status'];
    }

    // Have parent class do the iterating.
    return parent::map($batch, $target_account);
  }

  /**
   * Override parent::configDefaults().
   */
  public function configDefaults() {
    return array(
      'status' => 1,
      'auto_username' => FALSE,
      'update_existing' => FALSE,
      'mappings' => array(),
    );
  }

  /**
   * Override parent::configForm().
   */
  public function configForm(&$form_state) {
    $form = array();

    $form['status'] = array(
      '#type' => 'radios',
      '#title' => t('Status'),
      '#description' => t('Select whether new users should be active or blocked.'),
      '#options' => array(0 => t('Blocked'), 1 => t('Active')),
      '#default_value' => $this->config['status'],
    );

    // Add a setting for auto-generating the user name.
    $form['auto_username'] = array(
      '#type' => 'checkbox',
      '#title' => t('Automatically generate username'),
      '#description' => t('Check this if you would like usernames to be automatically generated for new users based on the email address.'),
      '#default_value' => $this->config['auto_username'],
    );

    // @todo Implement true updating.
    $form['update_existing'] = array(
      '#type' => 'checkbox',
      '#title' => t('Update existing users'),
      '#description' => t('If an existing user is found for an imported user, update it. Existing users will be determined using mappings that are a "unique target".'),
      '#default_value' => $this->config['update_existing'],
    );
    return $form;
  }

  /**
   * Set target element.
   */
  public function setTargetElement(&$target_item, $target_element, $value) {
    $matches = array();
    if (preg_match('/cp:(.*):(.*)/', $target_element, $matches)) {
      // Nest content profile values in the object.
      $type_name = $matches[1];
      $field_name = $matches[2];

      if (!isset($target_item->content_profile_feeds)) {
        $target_item->content_profile_feeds = array();
      }
      if (!isset($target_item->content_profile_feeds[$type_name])) {
        $target_item->content_profile_feeds[$type_name] = array();
      }
      $target_item->content_profile_feeds[$type_name][$field_name] = array(
        array(
          'value' => $value,
        ),
      );
    }
    else {
      $target_item->$target_element = $value;
    }
  }

  /**
   * Return available mapping targets.
   */
  public function getMappingTargets() {
    $targets = array(
      'name' => array(
        'name' => t('User name'),
        'description' => t('Name of the user.'),
        'optional_unique' => TRUE,
       ),
      'mail' => array(
        'name' => t('Email address'),
        'description' => t('Email address of the user.'),
        'optional_unique' => TRUE,
       ),
      'created' => array(
        'name' => t('Created date'),
        'description' => t('The created (e. g. joined) data of the user.'),
       ),
      'pass' => array(
        'name' => t('Unencrypted Password'),
        'description' => t('The unencrypted user password.'),
      ),
    );
    if (module_exists('openid')) {
      $targets['openid'] = array(
        'name' => t('OpenID identifier'),
        'description' => t('The OpenID identifier of the user. <strong>CAUTION:</strong> Use only for migration purposes, misconfiguration of the OpenID identifier can lead to severe security breaches like users gaining access to accounts other than their own.'),
        'optional_unique' => TRUE,
       );
    }

    // Let other modules expose mapping targets.
    self::loadMappers();
    drupal_alter('feeds_user_processor_targets', $targets);

    // Add content profile field targets.
    foreach (content_profile_get_types('names') as $type_name => $label) {
      $type = content_types($type_name);
      foreach ($type['fields'] as $field) {
        $key = "cp:{$type_name}:{$field['field_name']}";
        $args = array(
          '@type' => $type['name'],
          '@field' => $field['widget']['label'],
        );
        $targets[$key] = array(
          'name' => t('@type: @field', $args),
          'description' => t('@type: @field', $args),
          'callback' => 'content_profile_feeds_set_target',
          'content_profile_type' => $type_name,
        );
      }
    }

    return $targets;
  }

  /**
   * Get id of an existing feed item term if available.
   */
  protected function existingItemId(FeedsImportBatch $batch, FeedsSource $source) {

    // Iterate through all unique targets and try to find a user for the
    // target's value.
    foreach ($this->uniqueTargets($batch) as $target => $value) {
      switch ($target) {
        case 'name':
          $uid = db_result(db_query("SELECT uid FROM {users} WHERE name = '%s'", $value));
          break;
        case 'mail':
          $uid = db_result(db_query("SELECT uid FROM {users} WHERE mail = '%s'", $value));
          break;
        case 'openid':
          $uid = db_result(db_query("SELECT uid FROM {authmap} WHERE authname = '%s' AND module = 'openid'", $value));
          break;
      }
      if ($uid) {
        // Return with the first nid found.
        return $uid;
      }
    }
    return 0;
  }
}
