<?php
/**
 * Implementation of hook_views_data().
 * @ingroup views
 */
function taguser_views_data() {

  // Put Tag User fields into their own group.
  $data['taguser']['table']['group'] = t('Tag User');

  // This table references the {node} and {users} table.
  $data['taguser']['table']['join'] = array(
    'node' => array(
      'left_field' => 'nid',
      'field' => 'content_nid',
    ),
    'users' => array(
      'left_field' => 'uid',
      'field' => 'tag_uid',
    ),
  );

  // Node ID field.
  $data['taguser']['content_nid'] = array(
    'title' => t('Tagged node'),
    'help' => t('The node containing tags to a user.'),
    'relationship' => array(
      'base' => 'node',
      'field' => 'nid',
      'handler' => 'views_handler_relationship',
      'label' => t('Node tagged with user'),
    ),
  );

  // User that was tagged.
  $data['taguser']['tag_uid'] = array(
    'title' => t('Tagged user'),
    'help' => t('The user that was tagged in a node.'),
    'relationship' => array(
      'base' => 'users',
      'field' => 'uid',
      'handler' => 'views_handler_relationship',
      'label' => t('User tagged in node'),
    ),
  );

  // User that created the tag.
  $data['taguser']['tagged_by_uid'] = array(
    'title' => t('User that created tag'),
    'help' => t('The user that created the tag. This will be the same as "Tagged user" if a user tagged themself.'),
    'relationship' => array(
      'base' => 'users',
      'field' => 'uid',
      'handler' => 'views_handler_relationship',
      'label' => t('User that created tag'),
    ),
  );

  // Example timestamp field.
  $data['taguser']['timestamp'] = array(
    'title' => t('Tag timestamp'),
    'help' => t('When this tag was made.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  // Return the data.
  return $data;
}

