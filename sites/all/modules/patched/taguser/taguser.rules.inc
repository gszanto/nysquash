<?php
/**
 * Implementation of hook_rules_event_info().
 * @ingroup rules
 */
function taguser_rules_event_info() {
  return array(
    'taguser_add' => array(
      'label' => t('User A tags content for user B.'),
      'module' => 'Tag user',
      'arguments' => array(
        'userA' => array(
          'type' => 'user',
          'label' => t('User A (tagger)'),
        ),
        'taggedContent' => array(
          'type' => 'node',
          'label' => t('The Tagged Content'),
        ),
        'userB' => array(
          'type' => 'user',
          'label' => t('User B (taggee)'),
        ),
      ),
    ),
  );
}

