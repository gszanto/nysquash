<?php

/**
 * Implementation of hook_form_alter().
 */
function csm_form_alter(&$form, $form_state, $form_id) {

  if ($form_id == 'node_type_form' && isset($form['identity']['type'])) {

    if (user_access('change ' . $form['#node_type']->type . ' submit messages')) {

      // If the current user has the correct permissions, add submit message
      // setting fields to the form
      $form['csm'] = array(
        '#type' => 'fieldset',
        '#title' => t('Submit message settings'),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
      );

      global $language;
      $current_lang = $language->language;

      // Add submit message subsections for each language
      foreach (language_list() as $lang => $details) {
        $form['csm'][$lang] = _csm_lang_fieldsets($lang, $form['#node_type']->type);
        $form['csm'][$lang]['#title'] = t($details->name);
        if ($lang == $current_lang) {
          $form['csm'][$lang]['#collapsed'] = FALSE;
        }
      }

    }

    if (user_access('change '. $form['#node_type']->type . ' creation page title')) {

      // If the current user has the correct permissions, add node creation
      // page title field setting fields to the form
      $form['create_form_title'] = array(
        '#type' => 'fieldset',
        '#title' => t('Node creation page title'),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
      );

      // Add create form title subsections for each language
      foreach (language_list() as $lang => $details) {
        $form['create_form_title'][$lang] = _csm_lang_fieldsets($lang, $form['#node_type']->type, 'title');
        $form['create_form_title'][$lang]['#title'] = t($details->name);
        if ($lang == $current_lang) {
          $form['create_form_title'][$lang]['#collapsed'] = FALSE;
        }
      }

      $form['create_form_title']['view']['token_help'] = $form['csm']['view']['token_help'] = array(
        '#title' => t('Replacement patterns'),
        '#type' => 'fieldset',
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
        );

      $form['create_form_title']['view']['token_help']['help'] = $form['csm']['view']['token_help']['help'] = array(
        '#value' => theme('token_help', 'node'),
        );

    }
  }
  elseif ($form['#id'] == 'node-form' && $form['nid']['#value'] == NULL) {

    global $language;

    // The node that the form refers to has been partly built. csm_nodeapi() has stored it in
    // the variable table. We need to retrieve it to get
    // token_replace to play nicely.
    $node = variable_get('csm_node_temp', NULL);
    variable_del('csm_node_temp');
    $title = variable_get('csm_form_title_' . $language->language . '_' . $form['type']['#value'], 'Create [type-name]');
    if ($title) {
      $title = token_replace($title, 'node', $node);
      drupal_set_title(check_plain($title));

    // Log a system message.
    watchdog('csm', '@type: node creation page title changed using Custom Submit Messages.', array('@type' => $node->type), WATCHDOG_NOTICE);
    }
  }
  return;
}

/**
 * Implementation of hook_nodeapi().
 */
function csm_nodeapi(&$node, $op, $a3 = NULL, $a4 = NULL) {
  if ($op == 'insert' || $op == 'update' || $op == 'delete') {
    $node->op = $op;
    _csm_variable_set('csm_node_temp', $node);
  }
  elseif ($op == 'prepare' && !isset($node->nid)) {
    // Adding a new node. Store the node in the variable table so that it can be retrieved
    // by hook_form_alter and token substitutions can be done properly on the title of
    // the page.
    _csm_variable_set('csm_node_temp', $node);
  }
  return;
}

/**
 * Implementation of hook_perm().
 */
function csm_perm() {

  // for each node type, add an option to change submit messages for that node type and change node
  // creation page title for that node type

  $node_types = node_get_types();

  foreach ($node_types as $key => $value) {
    $perm[] = 'change ' . $key . ' submit messages';
    $perm[] = 'change ' . $key . ' creation page title';
  }

  return $perm;
}

/**
 * Implementation of hook_message_alter().
 */
function csm_message_alter(&$messages) {

  // If $messages->messages['status'] doesn't exist then there are no status
  // messages and there's nothing to do
  if (!isset($messages->messages['status'])) {
    return;
  }

  // Search for submit messages and change any that are found

  // First create arrays containing messages that may need to be changed
  $created = $messages->contains('</em> has been created.', 'status');
  $updated = $messages->contains('</em> has been updated.', 'status');
  $deleted = $messages->contains('</em> has been deleted.', 'status');

  // Then parse through each array, double-checking for messages that need to be
  // changed and then changing them
  $message_types_to_check = array(created, updated, deleted);
  foreach ($message_types_to_check as $delta => $type) {
    if ($$type == FALSE) {
      continue; // There are no messages of type $$type so nothing to check
    }
    else {
      // Double check the messages
      foreach (${$type}['status'] as $delta_2 => $message_array) {
        // Check that the message does not start with 'The content type <em>'
        // If it doesn't then the message is relevant.
        if (!(strpos($message_array['message'], 'The content type <em>'))) {
          $relevant_messages[] = $message_array['index'];
        }
      }
    }
  }

  if (!isset($relevant_messages)) {
    // No relevant messages so nothing to change
    return;
  }

  // Change the messages:
  foreach ($relevant_messages as $delta => $index) {

    // Load the node that we saved in csm_nodeapi().
    $node = variable_get('csm_node_temp', NULL);
    variable_del('csm_node_temp');

    // Check the active language then loads the msg based on that.
    global $language;

    // Change the status message to the custom status message.
    $messages->messages['status'][$delta] = variable_get('csm_' . $node->op . '_msg_' . $language->language . '_' . $node->type, $messages->messages['status'][$delta]);
    $messages->messages['status'][$delta] = token_replace($messages->messages['status'][$delta], 'node', $node);

    // Log a system message.
    watchdog('csm', '@type: node @msg_type message changed using Custom Submit Messages.', array('@type' => $node->type, '@msg_type' => $node->op), WATCHDOG_NOTICE);
  }
  return;
}

/**
 * Custom variable_set() function that ensures the correct $node is set in the
 * variable table
 */
function _csm_variable_set($name, $value) {

  if (module_exists('nodecomment')) {
    if (variable_get($name, 'notset') !== 'notset') {
      // The variable has already been set so there's not need to set it again
      return;
    }
  }
  variable_set($name, $value);
  return;
}

/**
 * Provide the form sub-section for a given language.
 */
function _csm_lang_fieldsets($lang = NULL, $form_type, $type = 'message') {

  if ($lang == NULL) {
    global $language;
    $lang = $language->language;
  }

  $fields = array(
    '#type' => 'fieldset',
    '#title' => t($lang),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    );

  switch ($type) {
    case 'message':
      $fields['csm_insert_msg_' . $lang] = array(
        '#type' => 'textfield',
        '#title' => t('Create message'),
        '#default_value' => t(variable_get('csm_insert_msg_' . $lang . '_' . $form_type, '[type-name] <em>[title]</em> has been created.')),
        '#description' => 'Enter the message you want to be shown to your users when they create a node of this type.',
        '#size' => 60,
        '#maxlength' => 128,
        '#required' => TRUE,
      );

      $fields['csm_update_msg_' . $lang] = array(
        '#type' => 'textfield',
        '#title' => t('Update message'),
        '#default_value' => t(variable_get('csm_update_msg_'. $lang . '_' . $form_type, '[type-name] <em>[title]</em> has been updated.')),
        '#description' => 'Enter the message you want to be shown to your users when they edit a node of this type.',
        '#size' => 60,
        '#maxlength' => 128,
        '#required' => TRUE,
      );

      $fields['csm_delete_msg_' . $lang] = array(
        '#type' => 'textfield',
        '#title' => t('Delete message'),
        '#default_value' => t(variable_get('csm_delete_msg_'. $lang . '_' . $form_type, '[type-name] <em>[title]</em> has been deleted.')),
        '#description' => 'Enter the message you want to be shown to your users when they delete a node of this type.',
        '#size' => 60,
        '#maxlength' => 128,
        '#required' => TRUE,
      );

      return $fields;
      break;
    case 'title':
      $fields['csm_form_title_' . $lang] = array(
        '#type' => 'textfield',
        '#title' => t('Node create form title'),
        '#default_value' => t(variable_get('csm_form_title_' . $lang . '_' . $form_type, 'Create [type-name]')),
        '#description' => 'Enter the title you want to be shown to your users on the node creation page.',
        '#size' => 60,
        '#maxlength' => 128,
        '#required' => TRUE,
      );

      return $fields;
      break;
  }
}

/**
 * Returns a list of all of the variables the module may have added to the
 * variable table. Used in csm.install when uninstalling the module to clear
 * the variables out of the variable table.
 */

function csm_variables() {

  $crud = array(insert_msg, delete_msg, form_title, update_msg);
  $node_types = node_get_types();

  // I think this only works if the initial Drupal install was in English - will
  // need to add in an extra clause or two if the initial install was not in
  // English
  if (module_exists('locale')) {
    $languages = locale_language_list();
  }
  else {
    $languages = array('en' => 'en');
  }

  foreach ($crud as $crud_key => $crud_value) {
    foreach ($node_types as $node_type_key => $node_type_value) {
      foreach ($languages as $language_key => $language_value) {
        $variables[] = 'csm_' . $crud_value . '_' . $language_key . '_' . $node_type_key;
      }
    }
  }

  if ($csm_node_temp = variable_get('csm_node_temp', FALSE)) {
    $variables[] = $csm_node_temp;
  }

  return $variables;
}