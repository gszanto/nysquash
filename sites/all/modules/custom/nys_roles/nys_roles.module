<?php

/**
 * @file
 * New York Squash - Roles
 */

/* ******************************************************************************
 * Drupal hooks
 * *****************************************************************************/

/**
 * Implementation of hook_nodeapi().
 */
function nys_roles_nodeapi(&$node, $op, $a3 = NULL, $a4 = NULL) {

  // Bail if we aren't dealing with a 'profile' node type, or if the operation isn't 'insert' or 'update'.
  if ($node->type != 'profile' || ($op != 'insert' && $op != 'update')) {
    return;
  }

  // The role we are looking for (and possibly adding/removing).
  $role_name = 'Member';

  // Load the profile's owner.
  $user = user_load($node->uid);

  // Check to see if the user has the role.
  $has_role = (array_search($role_name, $user->roles) === FALSE) ? FALSE : TRUE;

  // Load the value of the 'paid thru date' field as a timestamp, if available.
  $paid_thru = !empty($node->field_profile_paid_thru[0]['value']) ? strtotime($node->field_profile_paid_thru[0]['value']) : FALSE;

  // If the paid thru date is valid, but expired, invalidate it.
  if ($paid_thru && $paid_thru <= time()) {
    $paid_thru = FALSE;
  }

  // If the field is invalid, and the user has the role, remove the role.
  if (!$paid_thru && $has_role) {
    nys_roles_change($user, $role_name, 'remove');
  }

  // Or, if the field is not empty and valid, add the role.
  elseif ($paid_thru && !$has_role) {
    nys_roles_change($user, $role_name, 'add');
  }
}

/**
 * Implementation of hook_cron().
 */
function nys_roles_cron() {

  /**
   * Look up all users with an expired membership and remove their 'Member' role.
   */

  // Define the role name.
  $role = 'Member';

  // Get the role id.
  $rid = array_search('Member', user_roles(TRUE));

  // If a role id wasn't found, bail.
  if ($rid === FALSE) {
    return;
  }

  // Get database information about the 'paid thru date' field.
  $field = content_fields('field_profile_paid_thru');
  $db = content_database_info($field);
  $table = $db['table'];
  $column = $db['columns']['value']['column'];

  // Assemble a query that loads a list of user ids for users that have the role, and are expired.
  $query = 'SELECT DISTINCT(n.uid)
    FROM ' . $table . ' ctp
    LEFT JOIN node n ON ctp.nid=n.nid
    LEFT JOIN users_roles r ON n.uid=r.uid
    WHERE
      r.rid = 12
      AND
      ' . $column . ' <= NOW()';

  // Execute the query and build an array of user ids.
  $result = db_query($query);
  $uids = array();
  while ($row = db_fetch_array($result)) {
    if ($row['uid']) {
      $uids[] = $row['uid'];
    }
  }

  // Loop through the list of user ids, and remove the role.
  foreach ($uids as $uid) {
    db_query('DELETE FROM {users_roles} WHERE uid=%d AND rid=%d', $uid, $rid);
  }
}

/* ******************************************************************************
 * Helper functions
 * *****************************************************************************/

/**
 * Helper function for adding/removing a role from a user.
 *
 * @param $user
 *   The user account to change.
 * @param $role
 *   The role to add/remove.
 * @param $op
 *   The operation to perform ('add' or 'remove').
 */
function nys_roles_change($user, $role, $op) {

  // Load the list of roles.
  $roles = user_roles(TRUE);

  // Find the id of the role.
  $rid = array_search($role, $roles);

  // If a role id wasn't found, bail.
  if ($rid === FALSE) {
    return;
  }

  // Switch through the available operations.
  switch ($op) {

    // Add the role.
    case 'add':
      $user->roles[$rid] = $role;
      break;

    // Remove the role.
    case 'remove':
      unset($user->roles[$rid]);
      break;
  }

  // Save the user with the new roles.
  user_save($user, array('roles' => $user->roles));
}