Drupal.behaviors.nys_forms = function (context) {

  // This Javascript will hide the confirmation checkboxes that are added to the add-to-cart form (in hook_form_alter of this module), and display them in a popup dialog when the user clicks "Sign Up" instead.

  // Bail if the agreement wrapper doesn't exist.
  if (!$('#add_to_cart_agree_wrapper')[0]) {
    return;
  }

  // Set up the #add_to_cart_agree_wrapper div as a jQuery UI Dialog.
  $('#add_to_cart_agree_wrapper').dialog({
    autoOpen: false,
    modal: true,
    buttons: {
      'Continue': function() {

        // Close the dialog.
        $(this).dialog('close');

        // If the checkboxes are checked...
        if (nys_forms_signup_checkboxes()) {

          // Move the checkbox fields back to the original form as hidden fields.
          var skill_level_wrapper = '#edit-agree-skill-level-wrapper';
          var time_pref_wrapper = '#edit-agree-time-pref-wrapper';
          var skill_level_wrapper_clone = $(skill_level_wrapper).clone();
          var time_pref_wrapper_clone = $(time_pref_wrapper).clone();
          $(skill_level_wrapper).remove();
          $(time_pref_wrapper).remove();
          $('#uc-product-add-to-cart-form').append(skill_level_wrapper_clone);
          $('#uc-product-add-to-cart-form').append(time_pref_wrapper_clone);
          $(skill_level_wrapper).hide();
          $(time_pref_wrapper).hide();

          // Submit the form.
          $('#uc-product-add-to-cart-form').submit();
        }
      },
      'Cancel': function() {
        $(this).dialog('close');
      }
    },
  });

  // Present the dialog when the "Sign Up" button is clicked.
  $('#uc-product-add-to-cart-form .form-submit').click(function (event) {
    $('#add_to_cart_agree_wrapper').dialog('open');
  });

  // When user tries submitting the "Sign up" form, present dialog.
  $('#uc-product-add-to-cart-form').submit(function (event) {

    // Disable the form's normal submission if the checkboxes aren't checked.
    if (!nys_forms_signup_checkboxes()) {
      event.preventDefault();
    }
  });
};

/**
 * Function for testing the checkboxes to see if they exist and are checked.
 */
function nys_forms_signup_checkboxes() {

  // Return FALSE if either of the checkboxes exist and are unchecked.
  if ($('#edit-agree-skill-level').length != 0 && !$('#edit-agree-skill-level').is(':checked')) {
    return false;
  }
  if ($('#edit-agree-time-pref').length != 0 && !$('#edit-agree-time-pref').is(':checked')) {
    return false;
  }

  // Otherwise return TRUE.
  return true;
}
