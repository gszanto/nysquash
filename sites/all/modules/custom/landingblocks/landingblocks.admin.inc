<?php

/**
 * Creates array of blocks to be updated.
 *
 * This is where you can add blocks. You need a bid
 * and custom function to add a block.
 */
function landingblocks_block_array()
	{
		// Every item ends with a comma, even the last one.
	  return array(
			77 => landingblocks_join(),
			79 => landingblocks_awards(),		
			78 => landingblocks_volunteer(),				
			76 => landingblocks_eventlist(),	
			75 => landingblocks_women(),				
			74 => landingblocks_doubles(),				
			73 => landingblocks_add_events(),				
			83 => landingblocks_ny_clubs(),				
			84 => landingblocks_popular_club_search(),				
			85 => landingblocks_custom_club_search(),
			80 => landingblocks_blogs(),				
			81 => landingblocks_news(),				
		/*	82 => landingblocks_get_published(),			*/
		);
	}


/***************************************************************
 * All remaining functions each return html for a single block.
 *
 * They will not work, unless added to landingblocks_block_array().
 ***************************************************************/


/* ---------------------------- EVENT PAGE BLOCKS---------------------------- */

function landingblocks_eventlist() 	{
		$ret = '';
	/*	$ret .= theme('imagecache', 'sm_feature', 'kitteh.jpeg');*/
		$ret .= '<h2 class="title node-title">Events Calendar</h2>';
		$ret .= 'Tournaments, parties, pro matches, and more';
		$ret .= '<div class="more-links"><a href="/events/event-list" class="more">Browse Events</a></div>';
		return $ret; 	}


function landingblocks_women() 	{
		$ret = '';
/*		$ret .= theme('imagecache', 'sm_feature', '800.jpeg');*/
		$ret .= '<h2 class="title node-title">Women\'s Events</h2>'; /*Insert Block title */
		$ret .= 'Find great events for women'; /*Text of block */
		$ret .= '<div class="more-links"><a href="/events/event-list" class="more">Women\'s Events</a></div>'; /* Button text */
		return $ret; 	}

function landingblocks_doubles()  {
		$ret = '';
/*		$ret .= theme('imagecache', 'sm_feature', '700.jpg');*/
		$ret .= '<h2 class="title node-title">Doubles Events</h2>'; /*Insert Block title */
		$ret .= 'See all doubles event listings'; /*Text of block */
		$ret .= '<div class="more-links"><a href="/events/event-list" class="more">All About Doubles</a></div>'; /* Button text */
		return $ret; 	}

function landingblocks_add_events() 	{
		$ret = '';
/*		$ret .= theme('imagecache', 'sm_feature', 'kitty1.jpeg');*/
		$ret .= '<h2 class="title node-title">Add Your Own Events</h2>'; /*Insert Block title */
		$ret .= 'Now easier for squash pros and club managers to promote their events directly on our site'; /*Text of block */
		$ret .= '<div class="more-links"><a href="/events/event-list" class="more">Try It</a></div>'; /* Button text */
		return $ret; 	}



/* ---------------------------- ABOUT US BLOCKS---------------------------- */

function landingblocks_join()
	{
		$ret = '';
		$ret .= '<h2 class="title node-title">Join New York Squash</h2>';
		$ret .= 'Lots of great reasons why you should join New York Squash';
		$ret .= '<div class="more-links"><a href="/events/event-list" class="more">Learn more</a></div>';
		return $ret;
	}

function landingblocks_awards()
	{
		$ret = '';
		$ret .= '<h2 class="title node-title">New York Squash Awards</h2>';
		$ret .= 'Each year, the organization recognizes the efforts and talents of our members, on and off the court';
		$ret .= '<div class="more-links"><a href="/about/awards" class="more">Learn more</a></div>';
		return $ret;
	}

function landingblocks_volunteer()
	{
		$ret = '';
		$ret .= '<h2 class="title node-title">Volunteer with us</h2>';
		$ret .= 'From planning events to marketing to technology, there are lots of ways you can help the squash community';
		$ret .= '<div class="more-links"><a href="/events/event-list" class="more">Help out</a></div>';
		return $ret;
	}

/* ---------------------------- VENUE PAGE BLOCKS---------------------------- */

function landingblocks_ny_clubs()
	{
		$ret = '';
		$ret .= '<h2 class="title node-title">Clubs in the NYC Area</h2>';
		$ret .= 'From the five boroughs to Long Island, Westchester and Southern CT, the NY metro area is one of the best places to find a court or a game.';
		$ret .= '<div class="more-links"><a href="/events/event-list" class="more">Browse NYC</a></div>';
		return $ret;
	}
	
function landingblocks_popular_club_search()
	{
		$ret = '';
		$ret .= '<h2 class="title node-title">Popular Club Searches</h2>';
		$ret .= '';
		$ret .= '<div class="more-links"><a href="/events/event-list" class="more">Browse NYC</a></div>';
		return $ret;
	}
	
function landingblocks_custom_club_search()
	{
		$ret = '';
		$ret .= '<h2 class="title node-title">Custom Club Search</h2>';
		$ret .= '';
		$ret .= '<div class="more-links"><a href="/events/event-list" class="more">Advanced Search</a></div>';
		return $ret;
	}

/* ---------------------------- BLOG PAGE BLOCKS---------------------------- */

function landingblocks_blogs()
	{
		$ret = '';
		$ret .= '<h2 class="title node-title">Blogs</h2>';
		$ret .= 'Perspectives and opinions from pros, amateurs, juniors and everyone in between.';
		$ret .= '<div class="more-links"><a href="/blogs" class="more">Read Blogs</a></div>';
		return $ret;
	}

function landingblocks_news()
	{
		$ret = '';
		$ret .= '<h2 class="title node-title">News</h2>';
		$ret .= 'Keep up with the latest local and world news about the sport we all love.';
		$ret .= '<div class="more-links"><a href="/about/news" class="more">Get News</a></div>';
		return $ret;
	}	
/*
function landingblocks_get_published()
	{
		$ret = '';
		$ret .= '<h2 class="title node-title">Custom Club Search</h2>';
		$ret .= 'We've made easy for all registered users to launch their own blog. Start writing and get published.';
		$ret .= '<div class="more-links"><a href="/events/event-list" class="more">Learn More</a></div>';
		return $ret;
	}	*/