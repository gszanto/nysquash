<?php

/**
 * Implementation of hook_feeds_tamper_default().
 */
function nys_import_feeds_tamper_default() {
  $export = array();

  $feeds_tamper = new stdClass;
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'nysquash_user_import-gender-find_replace_female';
  $feeds_tamper->importer = 'nysquash_user_import';
  $feeds_tamper->source = 'Gender';
  $feeds_tamper->plugin_id = 'find_replace';
  $feeds_tamper->settings = array(
    'find' => 'F',
    'replace' => '2',
    'case_sensitive' => 0,
    'word_boundaries' => 0,
    'whole' => 0,
    'regex' => FALSE,
    'func' => 'str_ireplace',
  );
  $feeds_tamper->weight = 1;
  $feeds_tamper->description = 'Find replace female';
  $export['nysquash_user_import-gender-find_replace_female'] = $feeds_tamper;

  $feeds_tamper = new stdClass;
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'nysquash_user_import-gender-find_replace_male';
  $feeds_tamper->importer = 'nysquash_user_import';
  $feeds_tamper->source = 'Gender';
  $feeds_tamper->plugin_id = 'find_replace';
  $feeds_tamper->settings = array(
    'find' => 'M',
    'replace' => '1',
    'case_sensitive' => 0,
    'word_boundaries' => 0,
    'whole' => 0,
    'regex' => FALSE,
    'func' => 'str_ireplace',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Find replace male';
  $export['nysquash_user_import-gender-find_replace_male'] = $feeds_tamper;

  return $export;
}
