<?php

/**
 * @file
 * Feeds Tamper plugin that converts a "[first-name] [last-name]" string into a user id.
 */

$plugin = array(
  'form' => 'nys_import_realname_to_uid_form',
  'callback' => 'nys_import_realname_to_uid_callback',
  'name' => 'Real Name to UID',
  'multi' => 'loop',
  'category' => 'Custom',
);

function nys_import_realname_to_uid_form($importer, $element_key, $settings) {
  $form = array();
  $form['help']['#value'] = t('Converts a "[first-name] [last-name]" string into a user id.');
  return $form;
}

function nys_import_realname_to_uid_callback($source, $item_key, $element_key, &$field, $settings) {

  // Explode the field into an array of values.
  $fields = explode(',', $field);

  // Loop through the fields.
  foreach ($fields as &$string) {

    // Trim it.
    $string = trim($string);

    // If the field is numeric, skip it.
    if (is_numeric($string)) {
      continue;
    }

    // Attempt to look up the user's id in the {realname} table.
    $uid = db_result(db_query('SELECT uid FROM {realname} WHERE realname = "%s"', $string));

    // If a user id was returned, replace the field with it.
    if ($uid) {
      $string = $uid;
    }

    // If a user id wasn't found, alert the user.
    else {
      drupal_set_message('A user by the name of "' . $string . '" was not found.', 'warning');
    }
  }

  // Implode the fields.
  $field = implode(',', $fields);
}
