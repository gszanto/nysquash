<?php

/**
 * Implementation of hook_feeds_importer_default().
 */
function nys_import_feeds_importer_default() {
  $export = array();

  $feeds_importer = new stdClass;
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'nysquash_photo_import';
  $feeds_importer->config = array(
    'name' => 'NY Squash Photo Import',
    'description' => 'Imports photos from a CSV file.',
    'fetcher' => array(
      'plugin_key' => 'FeedsFileFetcher',
      'config' => array(
        'direct' => FALSE,
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsCSVParser',
      'config' => array(
        'delimiter' => ',',
        'no_headers' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'content_type' => 'photo',
        'input_format' => '0',
        'update_existing' => '0',
        'expire' => '-1',
        'mappings' => array(
          0 => array(
            'source' => 'Filename',
            'target' => 'field_photo_image',
            'unique' => FALSE,
          ),
          1 => array(
            'source' => 'Caption',
            'target' => 'title',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'Description',
            'target' => 'body',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'Published',
            'target' => 'status',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'Author',
            'target' => 'uid',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => 'People in photo',
            'target' => 'field_photo_users:uid',
            'unique' => FALSE,
          ),
          6 => array(
            'source' => 'Home Page Image',
            'target' => 'field_photo_home_image',
            'unique' => FALSE,
          ),
          7 => array(
            'source' => 'Promoted',
            'target' => 'promote',
            'unique' => FALSE,
          ),
        ),
        'author' => '4639',
        'authorize' => 0,
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 1,
  );
  $export['nysquash_photo_import'] = $feeds_importer;

  $feeds_importer = new stdClass;
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'nysquash_user_import';
  $feeds_importer->config = array(
    'name' => 'NY Squash User Import',
    'description' => 'Imports/updates users from a CSV file.',
    'fetcher' => array(
      'plugin_key' => 'FeedsFileFetcher',
      'config' => array(
        'direct' => FALSE,
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsCSVParser',
      'config' => array(
        'delimiter' => ',',
        'no_headers' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'ContentProfileFeedsUserProcessor',
      'config' => array(
        'update_existing' => 1,
        'status' => '1',
        'mappings' => array(
          0 => array(
            'source' => 'Email',
            'target' => 'mail',
            'unique' => 1,
          ),
          1 => array(
            'source' => 'Last Name',
            'target' => 'cp:profile:field_user_profile_lname',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'First Name',
            'target' => 'cp:profile:field_user_profile_fname',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'Middle Name',
            'target' => 'cp:profile:field_user_profile_mname',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'ID (usq)',
            'target' => 'cp:profile:field_profile_usq_id1',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => 'Category (usq)',
            'target' => 'cp:profile:field_profile_usq_category',
            'unique' => FALSE,
          ),
          6 => array(
            'source' => 'CategoryCode (usq)',
            'target' => 'cp:profile:field_profile_usq_category_code',
            'unique' => FALSE,
          ),
          7 => array(
            'source' => 'Status (usq)',
            'target' => 'cp:profile:field_profile_usq_status',
            'unique' => FALSE,
          ),
          8 => array(
            'source' => 'Membertype (usq)',
            'target' => 'cp:profile:field_profile_usq_membertype',
            'unique' => FALSE,
          ),
          9 => array(
            'source' => 'MemberTypeCode (usq)',
            'target' => 'cp:profile:field_profile_usq_membertypecode',
            'unique' => FALSE,
          ),
          10 => array(
            'source' => 'Autorenewal (usq)',
            'target' => 'cp:profile:field_profile_usq_autorenewal',
            'unique' => FALSE,
          ),
          11 => array(
            'source' => 'Address 1',
            'target' => 'cp:profile:field_profile_cck_address1',
            'unique' => FALSE,
          ),
          12 => array(
            'source' => 'Address 2',
            'target' => 'cp:profile:field_profile_cck_address2',
            'unique' => FALSE,
          ),
          13 => array(
            'source' => 'City',
            'target' => 'cp:profile:field_profile_cck_city',
            'unique' => FALSE,
          ),
          14 => array(
            'source' => 'State',
            'target' => 'cp:profile:field_profile_cck_province',
            'unique' => FALSE,
          ),
          15 => array(
            'source' => 'Postcode',
            'target' => 'cp:profile:field_profile_cck_postalcode',
            'unique' => FALSE,
          ),
          16 => array(
            'source' => 'Nation',
            'target' => 'cp:profile:field_profile_cck_country',
            'unique' => FALSE,
          ),
          17 => array(
            'source' => 'Work Phone',
            'target' => 'cp:profile:field_profile_workphone',
            'unique' => FALSE,
          ),
          18 => array(
            'source' => 'Home Phone',
            'target' => 'cp:profile:field_profile_homephone',
            'unique' => FALSE,
          ),
          19 => array(
            'source' => 'Mobile Phone',
            'target' => 'cp:profile:field_profile_mobilephone',
            'unique' => FALSE,
          ),
          20 => array(
            'source' => 'Fax',
            'target' => 'cp:profile:field_profile_fax',
            'unique' => FALSE,
          ),
          21 => array(
            'source' => 'Gender',
            'target' => 'cp:profile:field_profile_gender',
            'unique' => FALSE,
          ),
          22 => array(
            'source' => 'Spouse (USQ)',
            'target' => 'cp:profile:field_profile_usq_spouse',
            'unique' => FALSE,
          ),
          23 => array(
            'source' => 'District (usq)',
            'target' => 'cp:profile:field_profile_usq_districtname',
            'unique' => FALSE,
          ),
          24 => array(
            'source' => 'Homeclub (usq)',
            'target' => 'cp:profile:field_profile_usq_clubname',
            'unique' => FALSE,
          ),
          25 => array(
            'source' => 'Date of Birth',
            'target' => 'field_profile_birthdate',
            'unique' => FALSE,
          ),
          26 => array(
            'source' => 'PaidThru (usq)',
            'target' => 'field_profile_paid_thru',
            'unique' => FALSE,
          ),
          27 => array(
            'source' => 'MostRecentPaymentDate (usq)',
            'target' => 'field_usq_profile_recentpayment',
            'unique' => FALSE,
          ),
          28 => array(
            'source' => 'AdultRating (usq)',
            'target' => 'cp:profile:field_profile_usq_adultrating',
            'unique' => FALSE,
          ),
          29 => array(
            'source' => 'JuniorRating (usq)',
            'target' => 'cp:profile:field_profile_usq_juniorrating',
            'unique' => FALSE,
          ),
          30 => array(
            'source' => 'Cert1A (usq)',
            'target' => 'cp:profile:field_profile_usq_cert1a',
            'unique' => FALSE,
          ),
          31 => array(
            'source' => 'Cert1B (usq)',
            'target' => 'cp:profile:field_profile_usq_cert1b',
            'unique' => FALSE,
          ),
          32 => array(
            'source' => 'Cert2 (usq)',
            'target' => 'cp:profile:field_profile_usq_cert2',
            'unique' => FALSE,
          ),
          33 => array(
            'source' => 'Cert3 (usq)',
            'target' => 'cp:profile:field_profile_usq_cert3',
            'unique' => FALSE,
          ),
          34 => array(
            'source' => 'Cert4 (usq)',
            'target' => 'cp:profile:field_profile_usq_cert4',
            'unique' => FALSE,
          ),
          35 => array(
            'source' => 'RefereeClub (usq)',
            'target' => 'cp:profile:field_profile_usq_refereeclub',
            'unique' => FALSE,
          ),
          36 => array(
            'source' => 'RefereeState (usq)',
            'target' => 'cp:profile:field_profile_usq_refereestate',
            'unique' => FALSE,
          ),
          37 => array(
            'source' => 'RefereeRegion (usq)',
            'target' => 'cp:profile:field_profile_usq_refereeregion',
            'unique' => FALSE,
          ),
          38 => array(
            'source' => 'RefereeNation (usq)',
            'target' => 'cp:profile:field_profile_usq_refereenation',
            'unique' => FALSE,
          ),
          39 => array(
            'source' => 'RefereeWorld (usq)',
            'target' => 'cp:profile:field_profile_usq_refereeworld',
            'unique' => FALSE,
          ),
          40 => array(
            'source' => 'RefereeIntl (usq)',
            'target' => 'cp:profile:field_profile_usq_refereeintl',
            'unique' => FALSE,
          ),
          41 => array(
            'source' => 'Home Club(s)',
            'target' => 'cp:profile:field_profile_club',
            'unique' => FALSE,
          ),
          42 => array(
            'source' => 'Name Prefix',
            'target' => 'cp:profile:field_profile_name_prefix',
            'unique' => FALSE,
          ),
          43 => array(
            'source' => 'Name Suffix',
            'target' => 'cp:profile:field_profile_name_suffix',
            'unique' => FALSE,
          ),
          44 => array(
            'source' => 'Nickname',
            'target' => 'cp:profile:field_profile_nname',
            'unique' => FALSE,
          ),
          45 => array(
            'source' => 'Main Telephone',
            'target' => 'cp:profile:field_profile_mainphone',
            'unique' => FALSE,
          ),
          46 => array(
            'source' => 'As a player I rate myself',
            'target' => 'cp:profile:field_profile_self_rating',
            'unique' => FALSE,
          ),
          46 => array(
            'source' => 'AdminRights (usq)',
            'target' => 'cp:profile:field_profile_usq_adminrights',
            'unique' => FALSE,
          ),          
        ),
        'auto_username' => 1,
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 1,
  );
  $export['nysquash_user_import'] = $feeds_importer;

  return $export;
}
