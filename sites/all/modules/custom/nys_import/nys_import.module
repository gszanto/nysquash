<?php

/**
 * @file
 * New York Squash - Import
 */

/* ******************************************************************************
 * Includes
 * *****************************************************************************/

include_once('nys_import.features.inc');

/* ******************************************************************************
 * Ctools hooks
 * *****************************************************************************/

/**
 * Implements hook_ctools_plugin_directory().
 */
function nys_import_ctools_plugin_directory($module, $plugin) {
  if ($module == 'feeds_tamper') {
    return 'plugins';
  }
}

/* ******************************************************************************
 * Feeds hooks
 * *****************************************************************************/

/**
 * Implementation of hook_feeds_after_parse().
 */
function nys_import_feeds_after_parse($importer, $source) {

  // If the photo importer is running...
  if ($importer->id == 'nysquash_photo_import') {

    // If there are items being imported, loop through them.
    if (!empty($source->batch->items)) {
      foreach ($source->batch->items as &$item) {

        // Prepend the image import directory to the filename.
        $item['filename'] = 'sites/default/files/imports/images' . $item['filename'];

        // If an author id isn't set, set it to the default configured user.
        if (empty($item['author'])) {
          $item['author'] = $importer->config['processor']['config']['author'];
        }

        // If a Home Page Image isn't set, default it to 0.
        if (empty($item['home page image'])) {
          $item['home page image'] = 0;
        }
      }
    }
  }
}

/**
 * Implementation of hook_feeds_user_processor_targets_alter().
 */
function nys_import_feeds_user_processor_targets_alter(&$targets) {

  // Profile field: birthdate
  $targets['field_profile_birthdate'] = array(  // Field name in drupal.
    'name' => t('Profile: Date of Birth'),  // Label in the targets dropdown in Feeds.
    'description' => t('Profile: Date of Birth'),  // Description in Feeds.
    'callback' => 'nys_import_set_target_date',  // Name of the callback function that will process the value.
  );

  // Profile field: Paid Through Date
  $targets['field_profile_paid_thru'] = array(
    'name' => t('Profile: Paid Through Date'),
    'description' => t('Profile: Paid Through Date'),
    'callback' => 'nys_import_set_target_date',
  );

  // Profile field: Most Recent Payment Date
  $targets['field_usq_profile_recentpayment'] = array(
    'name' => t('Profile: Most Recent Payment Date'),
    'description' => t('Profile: Most Recent Payment Date'),
    'callback' => 'nys_import_set_target_datetime',
  );

  // Profile field: Home Club(s)
  $targets['field_profile_club'] = array(
    'name' => t('Profile: Home Club(s)'),
    'description' => t('Profile: Home Club(s)'),
    'callback' => 'nys_import_set_node_reference',
  );
}

/* ******************************************************************************
 * Feeds Tamper hooks
 * *****************************************************************************/



/* ******************************************************************************
 * Callbacks
 * *****************************************************************************/

/**
 * Convert a date string to the date format (YYYY-MM-DDTHH:MM:SS).
 */
function nys_import_set_target_date(&$account, $target, $value) {
  _nys_import_set_target_date($account, $target, $value, 'date');
}

/**
 * Convert a date string to the datetime format (YYYY-MM-DD HH:MM:SS).
 */
function nys_import_set_target_datetime(&$account, $target, $value) {
  _nys_import_set_target_date($account, $target, $value, 'datetime');
}

/**
 * Helper function for saving the date values.
 */
function _nys_import_set_target_date(&$account, $target, $value, $type) {

  // Convert the value to a timestamp.
  $timestamp = strtotime($value);

  // If the value is NULL, use that.
  if (!$timestamp) {
    $value = NULL;
  }

  // Otherwise, set the value.
  else {
    switch ($type) {
      case 'date':
        $value = date('Y-m-d\TH:i:s', $timestamp);
        break;
      case 'datetime':
        $value = date('Y-m-d H:i:s', $timestamp);
    }
  }

  // Add it to the content_profile_feeds array, to be processed in ContentProfileFeedsUserProcessor::process().
  if (!isset($account->content_profile_feeds)) {
    $account->content_profile_feeds = array();
  }
  if (!isset($account->content_profile_feeds['profile'])) {
    $account->content_profile_feeds['profile'] = array();
  }
  $account->content_profile_feeds['profile'][$target] = array(
    array(
      'value' => $value,
    ),
  );
}

/**
 * Save node ids to a node reference field.
 */
function nys_import_set_node_reference(&$account, $target, $value, $type) {

  // Explode the value into an array, delimited by double pipes ||.
  $nids = explode('||', $value);

  // Format the values array in a way that node references expect.
  $values = array();
  foreach ($nids as $nid) {
    $values[] = array(
      'nid' => $nid,
    );
  }

  // Add it to the content_profile_feeds array, to be processed in ContentProfileFeedsUserProcessor::process().
  if (!isset($account->content_profile_feeds)) {
    $account->content_profile_feeds = array();
  }
  if (!isset($account->content_profile_feeds['profile'])) {
    $account->content_profile_feeds['profile'] = array();
  }
  $account->content_profile_feeds['profile'][$target] = $values;
}
