<?php

/**
 * @file
 * NYSquash Start Time admin interface.
 */

/**
 * NYSquash Start Timesettings form callback
 */
function nys_start_time_settings_form(&$form_state) {

  // Set the page title.
  drupal_set_title('Start time settings');

  // Start time nodetype.
  $form['nys_start_time_nodetype'] = array(
    '#type' => 'select',
    '#title' => t('Start time node type'),
    '#description' => t('Select the node type you would like to apply special start time functionality to.'),
    '#options' => node_get_types('names'),
    '#default_value' => variable_get('nys_start_time_nodetype', NULL),
    '#required' => TRUE,
  );

  // Admin email address.
  $form['nys_start_time_email'] = array(
    '#type' => 'textfield',
    '#title' => t('Admin email address'),
    '#description' => t('Enter an email address that a confirmation notification should be sent to (in addition to the email that is sent to the player).'),
    '#default_value' => variable_get('nys_start_time_email', ''),
    '#required' => TRUE,
  );

  // Pass the form through the system_settings_form() function and return it.
  return system_settings_form($form);
}
