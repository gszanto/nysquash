<?php

/**
 * @file
 * New York Squash - Start time - Views integration
 */

/**
 * Implementation of hook_views_data().
 */
function nys_start_time_views_data() {

  // Define the {nys_start_time} table.
  $data['nys_start_time']['table']['group'] = t('NY Squash Start Time');

  // This table creates an 'implicit' relationship to the node table, so that when 'Node'
  // is the base table, the fields are automatically available 
  $data['nys_start_time']['table']['join'] = array(
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
     ),
  );

  // 
  $data['nys_start_time']['confirmed'] = array(
    'title' => t('Start time confirmation date'),
    'help' => t('The date that a player confirmed their start time.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
  );
  return $data;
}
