<?php

/**
 * Start time confirmation email
 */
function template_preprocess_nys_start_time_confirm_email(&$vars) {

  // Provide the base url for links
  global $base_url;
  $vars['base_url'] = $base_url;

  // Load the player and node objects from the params.
  $vars['player'] = $vars['params']['player'];
  $vars['node'] = $vars['params']['node'];

  // Load the venue node.
  if (!empty($vars['node']->field_start_time_venue[0]['nid'])) {
    $vars['venue'] = node_load($vars['node']->field_start_time_venue[0]['nid']);

    // Assemble the address.
    if (!empty($vars['venue']->field_venue_loc[0])) {
      $address_pieces = array(
        'name',
        'street',
        'additional',
        'city',
        'province_name',
        'postal_code',
        'country_name',
      );
      foreach ($address_pieces as $piece) {
        if (!empty($vars['venue']->field_venue_loc[0][$piece])) {
          $address[] = $vars['venue']->field_venue_loc[0][$piece];
        }
      }
      $vars['address'] = implode(', ', $address);
    }
  };

  // Load the start time and create a date and time variable from it.
  $start_time = $vars['node']->field_start_time_start[0]['value'];
  $start_timezone = $vars['node']->field_start_time_start[0]['timezone'];
  $start_timezone_db = $vars['node']->field_start_time_start[0]['timezone_db'];
  $date_object = new DateTime($start_time, new DateTimeZone($start_timezone_db));
  $date_object->setTimezone(new DateTimeZone($start_timezone));
  $start_time = $date_object->format('F jS, Y g:i a');
  $vars['date'] = date_format_date($date_object, 'custom', 'F jS, Y');
  $vars['time'] = date_format_date($date_object, 'custom', 'g:i a');
}
