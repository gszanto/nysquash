<?php

/**
 * @file
 * Defines functionality for the squash module, via constants.
 *
 * This can always be built into the admin section of the website,
 *  but keeping them here is better for performance and decreases
 *  development time.
 */

/**
 * Season top teams block.
 */
define('CURRENT_SEASON_NID', 445);
define('NUMBER_OF_TOP_TEAMS_IN_BLOCK', 3);
define('TOP_TEAMS_BLOCK_BID', 56); // This will probably never change.

/**
 * Division top teams blocks.
 */
define('NUMBER_OF_TOP_TEAMS_IN_DIVISION_BLOCK', 3);
// divisions need to be an array (not a constant) and are declared
// in divisions.inc.

/**
 * Doubles specific stuff.
 */
define('NUMBER_OF_INDIVIDUAL_MATCHES_PER_NIGHT_FOR_DOUBLES', 1);

/**
 * Singles specific stuff.
 */
define('DEFAULT_NUMBER_OF_INDIVIDUAL_MATCHES_PER_NIGHT_FOR_SINGLES', 4);
