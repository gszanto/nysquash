<?php
// $ID$

/**
 * @file
 * Administration page callbacks for the squash module.
 */

/**
 * Form builder. Configure squash leagues.
 *
 * @ingroup forms
 * @see system_settings_form().
 */ 
function squash_form_settings() {
   $form = array();

  // Get an array of node types with internal names as keys and
  // "friendly names" as values. E.g.,
  // array('page' => 'Page', 'story' => 'Story')

  $options = node_get_types('names');

  $form['squash_node_types'] = arrary(
    '#type' => 'checkboxes',
    '#title' => t('These content types are squash events'),
    '#options' => $options,
    '#default_value' => variable_get('squash_event_node_types', array('page')),
    '#description' => 't('Squash scoring will be applied to the following event content type(s).'),
  );

  return system_settings_form($form);

}
