<?php

/**
 * @file
 * This is the custom template for NY Squash
 */
?>
<table width="95%" border="0" cellspacing="0" cellpadding="1" align="center" style="font-family: verdana, arial, helvetica; font-size: small;">
  <!-- begin row 1 of invoice -->
  <tr>
	<!-- site logo -->
	<td width="40%"><br />
	  <img src="http://nysquash.net/sites/default/files/logos/NYS_Logo_Black.jpg" width=160px" /><br /><br />
		<div style="padding: 1em;">
		  <?php echo $store_address; ?><br />
		  <br />
		  Phone: <?php echo $store_phone; ?><br />
		  Fax: <?php echo $store_fax; ?><br />
		  <?php print $store_email; ?><br />
		  nysquash.com
		</div><br />
	</td>
	<!-- site address, phone, fax, email, url -->
	<td width="1%">
		&nbsp;
	</td>
	<td style="width:59%; vertical-align:bottom !important;">
		<div style="font-family: verdana, arial, helvetica; font-size: 24px; font-weight: bold; padding-bottom:10px;"><?php print $order_first_name . ' ' . $order_last_name; ?></div>
	</td>
  </tr>
  <!-- end row 1 of invoice -->
  <!-- begin row 2 of invoice -->
  <tr style="padding-top:20px">
		<td style="border-top: 1px solid; width: 40%; padding: 10px 0 0 10px">
			<table width="100%">
			  <tr>
				<!-- billing info (and shipping if shippable) -->
				<td style="font-size: small">
					<b><?php echo t('Billing !!Information:'); ?></b><br />
					<?php echo $order_billing_address; ?><br />
					<?php if (!empty($order_billing_phone)) : ?>
					Phone: <?php echo $order_billing_phone; ?><br />
					<?php endif; ?>
					Email: <?php echo $order_email; ?>
				</td>
				<?php if (uc_order_is_shippable($order)) { ?>
				<td>
				  <b><?php echo t('Shipping Address:'); ?></b><br />
				  <?php echo $order_shipping_address; ?><br />
				</td>
				<?php } ?>
			  </tr>
			</table>
			</td>
		<td style="border-top: 1px solid; width: 1%;">&nbsp;</td>
		<td style="border-top: 1px solid; width: 59%;">
			  <table cellspacing="0" style="text-align:center; width:100%;">
				<tr>
				  <td style="border: 1px solid; border-right: none; border-top: none; padding: 45px;">
					<b><?php echo t('Order&nbsp;#'); ?></b>
					<?php echo $order_link; ?>
				  </td>
				  <td style="border: 1px solid #000; border-top: none; background-color:#000; color:#fff; padding: 30px;">
					<b><?php echo t('Order&nbsp;Total'); ?></b><br />
					<b><?php echo $order_total; ?></b>
				  </td>
				  <td style="border: 1px solid; border-top: none; padding: 30px;">
					<b><?php echo t('Invoice&nbsp;Date'); ?></b><br />
					<b><?php echo $order_date_created; ?></b>
				  </td>
				</tr>
			  </table>
		</td>
  </tr>
  <!-- begin row 2 of invoice -->
  <!-- begin row 3 of invoice -->
  <tr>
	  <!-- begin order items table -->
	<td colspan="3">

	<table cellspacing="0" cellpadding="5" width="100%" style="margin-top: 15px; font-family: verdana, arial, helvetica; font-size: small;">

		  <?php if (is_array($order->products)) {
			$context = array(
			  'revision' => 'formatted',
			  'type' => 'order_product',
			  'subject' => array(
				'order' => $order,
			  ),
			);
			foreach ($order->products as $product) {
			  $price_info = array(
				'price' => $product->price,
				'qty' => $product->qty,
			  );
			  $context['subject']['order_product'] = $product;
			  $context['subject']['node'] = node_load($product->nid);
			  ?>
		<thead style="text-align:left">
		  <th>Description</th>
		  <th>Qty</th>    					  
		  <th>Price</th>
		  <th>Sub-total</th>    					  
		</thead>
		<tr>
			<td style="border-top:1px solid" width="40%">
			  <b><?php echo $product->title; ?></b>
			</td>
			<td style="border-top:1px solid" width="20%">
			  <?php echo $product->qty; ?>
			</td>
			<td style="border-top:1px solid" width="20%">
			  <?php echo uc_price($price_info, $context); ?>
			</td>
			<td style="border-top:1px solid" width="20%">
			  <?php $qty = $product->qty;
			  $price_each = $product->price;
			  $ext_price = $qty * $price_each; 
			  print $ext_price; ?>
			</td>                            
		  </tr>
		  <?php }
			  }?>
		</table>

	  </td>
	  <!-- end order items table -->
  </tr>
  <!-- begin row 3 of invoice -->  
</table>
