Drupal.uc_order_attributes = {
  
  // Open the attribute edit form in a modal dialog.
  modal: function(order_product_id, key) {
    var options = {
      url : Drupal.settings.basePath + 'admin/store/uc_order_attributes/' + order_product_id + '/' + key,
      width : 700,
      onSubmit: this.refresh,
    };
    Drupal.modalFrame.open(options);
    return false;
  },
  
  // Refresh the attributes list in the order products list.
  refresh: function(args, statusMessages) {
    if (args['order_product_id']) {
      
      // Update the attributes column with text.
      $('#attributes-' + args['order_product_id']).empty().append(args['attributes']);
      
      // Update the product data hidden field.
      $('input[name="products\\[' + args['product_key'] + '\\]\\[data\\]"]').val(args['data_serialized']);
    }
  },
};