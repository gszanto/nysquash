$(document).ready(function(){

  // display descriptions when service buttons are clicked
  $('#ucan li').live('click',function(e){
    var $this = $(this);
    
    // don't do anything if we're clicking on the active button
    if (!$this.hasClass('active')) {
    
      // get current class
      var currentClass = $(this).attr('id');
      
      // reset all buttons / descriptions to off-state
      
      $('#ucan li').removeClass('active').find('.off').show(0);
      $('.desc div').hide(0);

      
      // make current button active
      $this.addClass('active').find('.off').hide();
              
      // make current description active
      $('.desc .'+currentClass).show(0);
    
    }
  });
});