//toying with the notion of hover tips using the field-level help text fields instead of the node form "help blocks" 
$(document).ready(function(){
  if($('body').hasClass('page-node-add-event')) {
	//$('.sidebar').hide();
	$('table.sticky-header').remove();
	$('fieldset:has(div.description)', 'div:has(div.description)', 'table:has(div.description)').addClass('has-tip').css('position', 'relative');
	//$('fieldset.has-tip > legend, table.has-tip > th, div.has-tip label').after('<span class="get-tip">?</span>');
	//$('div.description').addClass('form-tip').hide();
	$('.get-tip').bind('mouseenter mouseleave',function(){$(this).closest('div, fieldset').children('.form-tip').slideToggle('fast', 'linear')});
	}
	
	$('.addthis_default_style').fadeIn('fast');
	$('ul#nice-menu-1').fadeIn('fast');
  });
  
/**
 * IE body classes
 */
Drupal.behaviors.notIE = function(context) {
  if ($.browser.msie) {
    $('html').addClass('ie');
	  var version = parseInt($.browser.version, 10);
	  $('html').addClass('ie'+version);
	}
}

/**
 * equal heights
 */
Drupal.behaviors.equalHeights = function(context) {
  
  if ($('.sidebar-second #content').height() > $('.region-sidebar-second .section').height()) {
  		$('.region-sidebar-second .section').css('height', $('.sidebar-second #content').height()); 
  	}
  	
  	
  if ($('.group-blogs').height() > $('.group-events').height()) {
 		$('.group-events').css('height', $('.group-blogs').height()); 
 	}

	//$('.landing-page .landing-item, .landing-view .views-row').equalHeights();
  $blog_height = $('.profile-photos').height() + $('.profile-videos').height();
  if($('.profile-page #block-views-blog_archive-block_1').height() < $blog_height) {
    $('.profile-page #block-views-blog_archive-block_1').css('height', $blog_height + 'px');
  }

}

/**
 * Bio 
 */
Drupal.behaviors.squashBio = function(context) {
  $('.bio .showbio').click(function(e) {
    e.preventDefault();
    $(this).toggleText('Show more', 'Show less').parent().toggleClass('open');
  });
  
  $('.overview .showdesc').click(function(e) {
    e.preventDefault();
    $(this).toggleText('Show more', 'Show less').parent().toggleClass('open');
  });
  
  
  jQuery.fn.toggleText = function (value1, value2) {
      return this.each(function () {
          var $this = $(this),
              text = $this.text();
   
          if (text.indexOf(value1) > -1)
              $this.text(text.replace(value1, value2));
          else
              $this.text(text.replace(value2, value1));
      });
  };
}

/**
 * region count
 */
Drupal.behaviors.regionCount = function(context) {
		var count = $('.region-hightlight .block').length;
		
		$('.region-highlight').addClass(count+'-blocks');

}

/**
 * Show/hide default text in search fields.
 */
Drupal.behaviors.searchFields = function(context) {

  // Load all the search fields.
  var site_search = $('#pre-header #edit-nys-mini-search-block-form-1');
  var user_first = $('#pre-header #edit-field-user-profile-fname-value');
  var user_last = $('#pre-header #edit-field-user-profile-lname-value');
  var user_name = $('#pre-header #edit-name');

  // Set the default values.
  var site_search_default = 'Search Site';
  var user_first_default = 'First Name';
  var user_last_default = 'Last Name';
  var user_name_default = 'Email or username';

  // Set initial text to the defaults.
  site_search.val(site_search_default);
  user_first.val(user_first_default);
  user_last.val(user_last_default);
  user_name.val(user_name_default);

  // If the user clicks on any of the field, and it is set to the default text, empty it.
  site_search.click(function() {
    if ($(this).val() == site_search_default) {
      $(this).val('');
    }
  });
  user_first.click(function() {
    if ($(this).val() == user_first_default) {
      $(this).val('');
    }
  });
  user_last.click(function() {
    if ($(this).val() == user_last_default) {
      $(this).val('');
    }
  });
  user_name.click(function() {
    if ($(this).val() == user_name_default) {
      $(this).val('');
    }
  });

  // If the user blurs focus from a field, and it is empty, set it to the default text.
  site_search.blur(function() {
    if ($(this).val() == '') {
      $(this).val(site_search_default);
    }
  });
  user_first.blur(function() {
    if ($(this).val() == '') {
      $(this).val(user_first_default);
    }
  });
  user_last.blur(function() {
    if ($(this).val() == '') {
      $(this).val(user_last_default);
    }
  });
  user_name.blur(function() {
    if ($(this).val() == '') {
      $(this).val(user_name_default);
    }
  });

  // If the search button is clicked, and any of the defaults are still set, empty them.
  $('#pre-header #search-block-form #edit-submit-3').click(function() {
    if (site_search.val() == site_search_default) {
      site_search.val('');
    }
  });
  $('#pre-header #edit-submit-user-list').click(function() {
    if (user_first.val() == user_first_default) {
      user_first.val('');
    }
    if (user_last.val() == user_last_default) {
      user_last.val('');
    }
    if (user_name.val() == user_name_default) {
      user_name.val('');
    }
  });
}
/**
 * End of function Drupal.behaviors.searchFields
 */

/*-------------------------------------------------------------------- 
 * JQuery Plugin: "EqualHeights"
 * by:	Scott Jehl, Todd Parker, Maggie Costello Wachs (http://www.filamentgroup.com)
 *
 * Copyright (c) 2008 Filament Group
 * Licensed under GPL (http://www.opensource.org/licenses/gpl-license.php)
 *
 * Description: Compares the heights or widths of the top-level children of a provided element 
 		and sets their min-height to the tallest height (or width to widest width). Sets in em units 
 		by default if pxToEm() method is available.
 * Dependencies: jQuery library, pxToEm method	(article: 
		http://www.filamentgroup.com/lab/retaining_scalable_interfaces_with_pixel_to_em_conversion/)							  
 * Usage Example: $(element).equalHeights();
  		Optional: to set min-height in px, pass a true argument: $(element).equalHeights(true);
 * Version: 2.0, 08.01.2008
--------------------------------------------------------------------*/

$.fn.equalHeights = function(px) {
	$(this).each(function(){
		var currentTallest = 0;
		$(this).children().each(function(i){
			if ($(this).height() > currentTallest) { currentTallest = $(this).height(); }
		});
		//if (!px || !Number.prototype.pxToEm) currentTallest = currentTallest.pxToEm(); //use ems unless px is specified
		// for ie6, set height since min-height isn't supported
		if ($.browser.msie && $.browser.version == 6.0) { $(this).css({'height': currentTallest}); }
		$(this).css({'min-height': currentTallest}); 
	});
	return this;
};