<?php
// $Id: template.php,v 1.21 2009/08/12 04:25:15 johnalbin Exp $

/**
 * @file
 * Contains theme override functions and preprocess functions for the theme.
 *
 * ABOUT THE TEMPLATE.PHP FILE
 *
 *   The template.php file is one of the most useful files when creating or
 *   modifying Drupal themes. You can add new regions for block content, modify
 *   or override Drupal's theme functions, intercept or make additional
 *   vars available to your theme, and create custom PHP logic. For more
 *   information, please visit the Theme Developer's Guide on Drupal.org:
 *   http://drupal.org/theme-guide
 *
 * OVERRIDING THEME FUNCTIONS
 *
 *   The Drupal theme system uses special theme functions to generate HTML
 *   output automatically. Often we wish to customize this HTML output. To do
 *   this, we have to override the theme function. You have to first find the
 *   theme function that generates the output, and then "catch" it and modify it
 *   here. The easiest way to do it is to copy the original function in its
 *   entirety and paste it here, changing the prefix from theme_ to squashv3_.
 *   For example:
 *
 *     original: theme_breadcrumb()
 *     theme override: squashv3_breadcrumb()
 *
 *   where squashv3 is the name of your sub-theme. For example, the
 *   zen_classic theme would define a zen_classic_breadcrumb() function.
 *
 *   If you would like to override any of the theme functions used in Zen core,
 *   you should first look at how Zen core implements those functions:
 *     theme_breadcrumbs()      in zen/template.php
 *     theme_menu_item_link()   in zen/template.php
 *     theme_menu_local_tasks() in zen/template.php
 *
 *   For more information, please visit the Theme Developer's Guide on
 *   Drupal.org: http://drupal.org/node/173880
 *
 * CREATE OR MODIFY vars FOR YOUR THEME
 *
 *   Each tpl.php template file has several vars which hold various pieces
 *   of content. You can modify those vars (or add new ones) before they
 *   are used in the template files by using preprocess functions.
 *
 *   This makes THEME_preprocess_HOOK() functions the most powerful functions
 *   available to themers.
 *
 *   It works by having one preprocess function for each template file or its
 *   derivatives (called template suggestions). For example:
 *     THEME_preprocess_page    alters the vars for page.tpl.php
 *     THEME_preprocess_node    alters the vars for node.tpl.php or
 *                              for node-forum.tpl.php
 *     THEME_preprocess_comment alters the vars for comment.tpl.php
 *     THEME_preprocess_block   alters the vars for block.tpl.php
 *
 *   For more information on preprocess functions and template suggestions,
 *   please visit the Theme Developer's Guide on Drupal.org:
 *   http://drupal.org/node/223440
 *   and http://drupal.org/node/190815#template-suggestions
 */

/**
 * Implementation of HOOK_theme().
 */
function squashv3_theme(&$existing, $type, $theme, $path) {
  $hooks = zen_theme($existing, $type, $theme, $path);
  // Add your theme hooks like this:
  /*
  $hooks['hook_name_here'] = array( // Details go here ); */
//$hooks['event_node_form'] = array(
//  'arguments' => array('form' => NULL),
//);
  // @TODO: Needs detailed comments. Patches welcome!
  return $hooks;
}

/**
 * Override or insert vars into all templates.
 *
 * @param $vars
 *   An array of vars to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered (name of the .tpl.php file.)
 */
/* -- Delete this line if you want to use this function
function squashv3_preprocess(&$vars, $hook) {
  $vars['sample_variable'] = t('Lorem ipsum.');
}
// */

/**
 * Override or insert vars into the page templates.
 *
 * @param $vars
 *   An array of vars to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("page" in this case.)
 */
/* -- Delete this line if you want to use this function
function squashv3_preprocess_page(&$vars, $hook) {
  $vars['sample_variable'] = t('Lorem ipsum.');
}
// */

/**
 * Override or insert vars into the node templates.
 *
 * @param $vars
 *   An array of vars to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("node" in this case.)
 */
/* -- Delete this line if you want to use this function

function squashv3_preprocess_node(&$vars, $hook) {
  $vars['sample_variable'] = t('Lorem ipsum.');

  // Optionally, run node-type-specific preprocess functions, like
  // squashv3_preprocess_node_page() or squashv3_preprocess_node_story().
  $function = __FUNCTION__ . '_' . $vars['node']->type;
  if (function_exists($function)) {
    $function($vars, $hook);
  }
}
// */

/**
 * Override or insert vars into the comment templates.
 *
 * @param $vars
 *   An array of vars to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("comment" in this case.)
 */
/* -- Delete this line if you want to use this function
function squashv3_preprocess_comment(&$vars, $hook) {
  $vars['sample_variable'] = t('Lorem ipsum.');
}
// */

/**
 * Override or insert vars into the block templates.
 *
 * @param $vars
 *   An array of vars to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("block" in this case.)
 */
/* -- Delete this line if you want to use this function
function squashv3_preprocess_block(&$vars, $hook) {
  $vars['sample_variable'] = t('Lorem ipsum.');
}
// */

function squashv3_addthis_button($node, $teaser) {
  $button = '<div class="addthis_toolbox addthis_default_style">
    <div class="custom_images">
      <a addthis:url="'.url('node/'. $node->nid, array('absolute' => 1) ).'" addthis:title="'.$node->title.'"  class="addthis_button_twitter"><img src="'. $GLOBALS[base_url].'/sites/all/themes/squashv3/images/twitter.png" width="16" height="16" alt="Tweet this" /></a>
      <a addthis:url="'.url('node/'. $node->nid, array('absolute' => 1) ).'" addthis:title="'.$node->title.'"  class="addthis_button_facebook"><img src="'. $GLOBALS[base_url].'/sites/all/themes/squashv3/images/facebook.png" width="16" height="16" alt="Share on Facebook" /></a>
      <a addthis:url="'.url('node/'. $node->nid, array('absolute' => 1) ).'" addthis:title="'.$node->title.'"  class="addthis_button_print"><img src="'. $GLOBALS[base_url].'/sites/all/themes/squashv3/images/print_icon.gif" width="16" height="16" alt="Print this" /></a>
      <a addthis:url="'.url('node/'. $node->nid, array('absolute' => 1) ).'" addthis:title="'.$node->title.'"  class="addthis_button_email"><img src="'. $GLOBALS[base_url].'/sites/all/themes/squashv3/images/mail_icon.png" width="16" height="16" alt="Email this" /></a>
    </div>
  </div>
  <script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#username=nysquash"></script>';

  return $button;
}

//make the theme system aware of page-nodetype.tpl.php files
function squashv3_preprocess_page(&$vars) {
  if  ($node = menu_get_object()) {
    $vars['node'] = $node;
    $suggestions = array();
    $template_filename = 'page';
    $template_filename = $template_filename . '-' . $vars['node']->type;
    $suggestions[] = $template_filename;
    $vars['template_files'] = $suggestions;

    if (($node->type == "event")) {
      squashv3_removetab('Option prices', $vars);
    }
  }


  if($vars['home_row_featured_1']) {
   $vars['home_row_classes'] = 'home_row_region home_row_featured home_row_first';
  }
  //dsm($vars['classes_array']);

  if(arg(1) == 'event-calendar') {
  	$vars['classes_array'][] = ' section-calendar';
  }
  //dsm($vars);
  global $user;

  if(arg(0) == 'user' && is_numeric(arg(1)) && !arg(2)) {
    $vars['classes_array'][] = ' profile-page';
    $vars['template_file'] = 'page-user-profile';
    $account = user_load(arg(1));
    //give access to update user info if user logged in is viewing their own profile or is Webmaster role
    if($user->uid == $account->uid || (in_array('Webmaster', array_values($user->roles)))){
      $vars['acct_btns'] = '<span class="account-buttons">';
      $vars['acct_btns'] .= l(t('Edit Email/Username/Password'), 'user/'. $account->uid .'/edit', array('attributes' => array('class' => 'edit-user')));
      $vars['acct_btns'] .= l(t('Edit Profile Details'), 'user/'. $account->uid .'/profile/profile', array('attributes' => array('class' => 'edit-profile')));
      $vars['acct_btns'] .= '</span>';
      }


  }

  if(isset($vars['node'])) {
    $node = $vars['node'];
  	  	if(isset($node->field_page_class)) {
  	  	 	$vars['classes_array'][] = $node->field_page_class[0]['value'];
  	  	}
	}

  if (arg(0) == 'user' && arg(2) == "profile") {
   $uid = arg(1);
    drupal_goto('user/' . $uid);
  }
}

function squashv3_preprocess_views_view_fields(&$vars) {

}

function squashv3_removetab($label, &$vars) {
  $tabs = explode("\n", $vars['tabs']);
  $vars['tabs'] = '';

  foreach($tabs as $tab) {
    if(strpos($tab, '>' . $label . '<') === FALSE) {
      $vars['tabs'] .= $tab . "\n";
    }
  }
}

function squashv3_preprocess_node(&$vars) {
  $node = $vars['node'];
  if (!$vars['teaser']) {
    switch ($node->type) {
      case 'event':
        $vars['photo'] = $node->field_event_image[0]['view'];

        // Query the database for a count of this event's signups.
        $vars['signups'] = db_result(db_query('SELECT COUNT(order_product_id) as signups FROM uc_order_products op LEFT JOIN uc_orders o ON op.order_id=o.order_id WHERE (o.order_status = "pending" OR o.order_status = "completed") AND nid=%d', $node->nid));
        if (!empty($vars['signups'])) {

          // We only want to link to the players list if the field_event_hide_entrants field on the event node is not set to 2.
          if ($node->field_event_hide_entrants[0]['value'] != 2) {
            $vars['signups_link'] = l($vars['signups'], 'events/' . $node->nid . '/entrants');
            $vars['view_players'] = '<p>' . l('View Players', 'events/' . $node->nid . '/entrants') . '</p>';
          }
        }

        // If start times were posted, set the variable to trigger the clock icon in node-event.tpl.php.
        if ($node->field_event_start_times_posted[0]['value'] == 2) {
          $vars['start_times_posted'] = TRUE;
        }

//			$vars['linktomore'] = $node->field_event_info_only_url[0]['url']['value'];
        if (!empty($node->field_event_alt_url[0]['value'])) {
          $vars['linktomore'] = l(t('More info'), $node->field_event_alt_url[0]['value'], array('attributes' => array('class' => 'more')));
        }
        if (count($node->field_event_image) > 1) {
          $vars['smallerphotos'] = '<div class="smallphotos">';
          for ($i = 1; $i <= count($node->field_event_image); $i++) {
            if ($node->field_event_image[$i]['filepath']) {
              $vars['smallerphotos'] .= theme('imagecache', '70x70_scale', $node->field_event_image[$i]['filepath']);
            }
          }
          $vars['smallerphotos'] .= '</div>';
        }

        // Assemble the event details information.
        $vars['details'] = '';
        foreach ($node->attributes as $attribute) {
          if ($attribute->default_label == 'Skill Level') {
            $skill_levels = array();
            foreach ($attribute->options as $option) {
              $skill_levels[] = $option->name;
            }
          }
        }
        if (!empty($skill_levels)) {
          $vars['details'] .= '<p>Skill level(s): ' . implode(', ', $skill_levels) . '</p>';
        }
        if (!empty($node->field_event_price_display[0]['value'])) {
          $vars['details'] .= $node->field_event_price_display[0]['value'];
        }
        if (!empty($node->field_event_close_date_display[0]['value'])) {
          $vars['details'] .= $node->field_event_close_date_display[0]['value'];
        }

        $vars['venues'] = views_embed_view('event_tabs', 'block_2', $node->nid);
        $vars['postboard'] = views_embed_view('event_tabs', 'block_6', $node->nid);
        $vars['photos'] = views_embed_view('gallery', 'block_2', $node->nid);
        $vars['videos'] = views_embed_view('gallery', 'block_1', $node->nid);

        $documents = views_get_view_result('event_tabs', 'block_12', $node->nid);
        if (!empty($documents[0]->node_data_field_event_document_field_event_document_fid)) {
          $vars['documents'] = views_embed_view('event_tabs', 'block_12', $node->nid);
        }

        $vars['instructors'] = views_embed_view('event_tabs', 'block_16', $node->nid);


        $camp_details = views_get_view_result('event_tabs', 'block_17', $node->nid);
        if (!empty($camp_details)) {
          $vars['camp_details'] = views_embed_view('event_tabs', 'block_17', $node->nid);
        }

        $vars['reviews'] = views_embed_view('event_review', 'block_1', $node->nid);

//			$vars['ussquash'] = module_invoke('block', 'block', '100');
        $vars['ussquash'] = module_invoke('block', 'block', 'view', '100');


        //$vars['start_times'] = $node->field_event_start_times[0]['value'];
        // User bio and wrapping div
        $vars['overview'] = $node->field_event_overview[0]['value'];
        /*  $vars['overview'] = '<div class="overview">';
            if (strlen($fulldesc) < 3000) {
            $vars['overview'] .= $fulldesc;
          } else {
            $shortdesc = truncate_utf8($fulldesc, 1000, TRUE, TRUE);
            $vars['overview'] .= '<div class="shortdesc">' . strip_tags($shortdesc, '<p>') . '</div>';
            $vars['overview'] .= '<div class="fulldesc">' . $fulldesc . '</div>';
            $vars['overview'] .= '<a href="#" class="showdesc">' . t('Show more') . '</a>';
          }
          $vars['overview'] .= '</div>'; */
        break;

      case 'agenda':
        $vars['agenda_details'] = views_embed_view('agenda_items', 'block_2', $node->nid);
        break;

      case 'venue':
        $vars['venue_logo'] = $node->field_venue_image[0]['view'];
        $vars['hours'] = $node->field_venue_hours[0]['value'];
        $vars['pros'] = views_embed_view('venue_tabs', 'block_3', $node->nid);
        $vars['photos'] = views_embed_view('venue_tabs', 'block_12', $node->nid);
        $vars['venue_players'] = views_embed_view('player_finder', 'block_1', $node->nid);
        break;
      case 'organization':
        $vars['logo'] = $node->field_organization_image[0]['view'];
        $vars['description'] = $node->field_organization_description[0]['value'];
        $vars['pros_org'] = views_embed_view('Organizations', 'block_4', $node->nid);

        if (!empty($node->field_organization_url[0]['value'])) {
          $vars['website'] = check_plain($node->field_organization_url[0]['value']);
        }
        break;
      case 'group':
        $vars['body'] = $node->content['og_mission']['#value'];
        $vars['group_info'] = views_embed_view('groups_tabs', 'block_1', $node->nid);
        $vars['events'] = views_embed_view('groups_tabs', 'block_2', $node->nid);
        $vars['blogs'] = views_embed_view('groups_tabs', 'block_3', $node->nid);
        $vars['photos'] = views_embed_view('groups_tabs', 'block_4', $node->nid);
        $vars['videos'] = views_embed_view('groups_tabs', 'block_7', $node->nid);
        break;
      case 'story':
        $share_links = $node->links['addthis']['title'];
        $name = $vars['name'];
        $date = format_date($node->created, 'short');
        $comments = $node->comment_count . t(' comments');

        $vars['story_photos'] = views_embed_view('story', 'block_1', $node->nid);
        $vars['info_box'] = '<div class="info-box">';
        $vars['info_box'] .= '<div class="author">' . t('By ') . $name . '</div>';
        $vars['info_box'] .= '<div class="date">' . $date . '</div>';
        $vars['info_box'] .= '<div class="comments">' . $comments . '</div>';
        $vars['info_box'] .= $share_links;
        $vars['info_box'] .= '</div>';


        unset($node->links['addthis']);
        $vars['links'] = theme_links($node->links, array('class' => 'links inline'));
        break;
      case 'team':
        drupal_set_title(t($vars['field_team_name'][0]['value']));
        $vars['photo'] = $vars['field_team_photo'][0]['view'];
        $vars['captain'] = $vars['field_team_captain'][0]['view'];
        $vars['cocaptain'] = $vars['field_team_cocaptain'][0]['view'];
        $vars['division'] = $vars['field_team_division'][0]['view'];
        $vars['homeclub'] = $vars['field_event_venue'][0]['view'];
        break;
      case 'season':
        drupal_set_title(t($vars['field_shared_organization'][0]['safe']['title'] . ' ' . $node->title . ' season'));
        break;
      case 'individual_match':
        $vars['division'] = $vars['field_match_division'][0]['view'];
        $vars['match'] = $vars['field_match_team_match'][0]['view'];
        $vars['num'] = t('Team Match #') . $vars['field_match_number'][0]['view'];
        $vars['home'] = $vars['field_match_home_team'][0]['view'];
        $vars['home_player'] = $vars['field_event_player1'][0]['view'] . $vars['field_event_player1_2nd'][0]['view'];
        $vars['away'] = $vars['field_match_away_team'][0]['view'];
        $vars['away_player'] = $vars['field_event_player2'][0]['view'] . $vars['field_event_player2_2nd'][0]['view'];

        //scores
        $vars['game_1_team_1'] = $vars['field_match_game1_team1'][0]['view'];
        $vars['game_1_team_2'] = $vars['field_match_game1_team2'][0]['view'];
        $vars['game_1_winner'] = $vars['field_match_winner_game1'][0]['view'];

        $vars['game_2_team_1'] = $vars['field_match_game2_team1'][0]['view'];
        $vars['game_2_team_2'] = $vars['field_match_game2_team2'][0]['view'];
        $vars['game_2_winner'] = $vars['field_match_winner_game2'][0]['view'];

        $vars['game_3_team_1'] = $vars['field_match_game3_team1'][0]['view'];
        $vars['game_3_team_2'] = $vars['field_match_game3_team2'][0]['view'];
        $vars['game_3_winner'] = $vars['field_match_winner_game3'][0]['view'];

        $vars['game_4_team_1'] = $vars['field_match_game4_team1'][0]['view'];
        $vars['game_4_team_2'] = $vars['field_match_game4_team2'][0]['view'];
        $vars['game_4_winner'] = $vars['field_match_winner_game4'][0]['view'];

        $vars['game_5_team_1'] = $vars['field_match_game5_team1'][0]['view'];
        $vars['game_5_team_2'] = $vars['field_match_game5_team2'][0]['view'];
        $vars['game_5_winner'] = $vars['field_match_winner_game5'][0]['view'];

        //points
        $vars['points_team1'] = $vars['field_match_points_team1'][0]['view'];
        $vars['points_team2'] = $vars['field_match_points_team2'][0]['view'];

        //winner
        $vars['winner'] = $vars['field_match_winner'][0]['view'];
        break;
      case 'team_match':
        global $base_url;
        $vars['away_team_node'] = node_load($node->field_team_match_away_team[0]['nid']);
        $vars['home_team_node'] = node_load($node->field_team_match_home_team[0]['nid']);
        $vars['away'] = $vars['field_team_match_away_team'][0]['view'];
        $vars['division'] = $vars['field_team_match_division'][0]['view'];
        $vars['venue'] = $vars['field_team_match_venue'][0]['view'];
        $vars['date'] = $vars['field_team_match_datetime'][0]['view'];
        $vars['home'] = l($vars['home_team_node']->field_team_name[0]['value'], $vars['home_team_node']->path);
        //Changed to use the (linked) field_team_name from the ref node instead of the node title
        //$vars['home'] = $vars['field_team_match_home_team'][0]['view'];
        $vars['home_player'] = $vars['field_event_player1'][0]['view'] . $vars['field_event_player1_2nd'][0]['view'];
        $vars['away'] = l($vars['away_team_node']->field_team_name[0]['value'], $vars['away_team_node']->path);
        //Changed to use the (linked) field_team_name from the ref node instead of the node title
        //$vars['away'] = $vars['field_team_match_away_team'][0]['view'];
        $vars['away_player'] = $vars['field_event_player2'][0]['view'] . $vars['field_event_player2_2nd'][0]['view'];
        if ($vars['field_team_match_winner'][0]['view'] != NULL) {
          $vars['winner'] = '<span class="winner">' . t('Winner') . '</span>' . $vars['field_team_match_winner'][0]['view'];
        }
        if ($vars['field_team_match_loser'][0]['view'] != NULL) {
          $vars['loser'] = '<span class="loser">' . t('Loser') . '</span>' . $vars['field_team_match_loser'][0]['view'];
        }
        break;
      case 'profile':
        if (arg(0) == 'node' && arg(1) == $node->nid && arg(2) == "") {
          // If we are postitive we are on node/N where N is the NID of a profile
          // node, then redirect to the user profile page instead.
          drupal_goto('user/' . $node->uid);
        }
        break;
      case 'video' :
        //dsm($node);
        break;
    }
  }
}

function squashv3_preprocess_block(&$vars) {
 // $vars['block_count'] = count(block_list($vars['block']->region));
}

function squashv3_preprocess_user_profile(&$vars) {

  // Get the user account object from the variables.
  $account = $vars['account'];

  // Set the title of the user profile to their real name.
  drupal_set_title($account->realname);

  // Load the content profile node for the account being viewed
  $profile_node = content_profile_load('profile', $account->uid);

  // User profile pic and wrapping div
  if (empty($profile_node->field_user_profile_photo[0]['filepath'])) {
    $profile_photo_path = 'sites/default/files/default-profile-photo-icon.png';
  } else {
    $profile_photo_path = $profile_node->field_user_profile_photo[0]['filepath'];
  }
  $vars['profile_photo'] = '<div class="profile-photo">' . theme('imagecache', '120x120_thumbnail_scale', $profile_photo_path) . '</div>';

  // User profile page image
  if (empty($profile_node->field_profile_page_img[0]['filepath'])) {
    $profile_image_path = 'sites/default/files/yellow-squash-court.jpg';
  } else {
    $profile_image_path = $profile_node->field_profile_page_img[0]['filepath'];
  }
  $vars['profile_image'] = '<div class="profile-image">' . theme('imagecache', '175w_image_scale', $profile_image_path) . '</div>';

  // User bio and wrapping div
  $fullbio = $profile_node->field_user_profile_bio[0]['value'];
  $vars['bio'] = '<div class="bio">';
  if (strlen($fullbio) < 550) {
    $vars['bio'] .= $fullbio;
  } else {
    $shortbio = truncate_utf8($fullbio, 550, TRUE, TRUE);
    $vars['bio'] .= '<div class="shortbio">' . strip_tags($shortbio, '<p>') . '</div>';
    $vars['bio'] .= '<div class="fullbio">' . $fullbio . '</div>';
    $vars['bio'] .= '<a href="#" class="showbio">' . t('Show more') . '</a>';
  }
  $vars['bio'] .= $vars['profile_image'];
  $vars['bio'] .= '</div>';

  // User's home club(s)
  if (!empty($profile_node->field_profile_club[0]['nid'])) {
    $vars['home_clubs'] = '<div class="home-clubs"><h3>' . t('Home Club(s):') .'</h3>';
    foreach($profile_node->field_profile_club as $club) {

      // Load the club node.
      $club_node = node_load($club['nid']);
      if ($club_node) {

        // Add the club to the list.
        $vars['home_clubs'] .= l($club_node->title, 'node/' . $club_node->nid);
      }
    }
    $vars['home_clubs'] .= '</div>';
  }
}


/** Override filter.module's theme_filter_tips() function to disable tips display.*/
function squashv3_filter_tips($tips, $long = FALSE, $extra = '')
{
return '';
}

function squashv3_filter_tips_more_info () {
return '';
}

function squashv3_menu_item_link($link) {
	// not sure why this was done but it was non-working for some users since their user page is at users/[user-raw] which strips .s and such
  // if($link['menu_name'] == 'menu-pre-header-menu') {
  //  global $user;
  //  if($link['title'] == 'My Account' && $link['href'] == 'user') {
  //    $link['href'] = 'users/' . $user->name;
  //  }
  // }

  if (empty($link['localized_options'])) {
    $link['localized_options'] = array();
  }

  return l($link['title'], $link['href'], $link['localized_options']);
}
