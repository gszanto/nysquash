<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language; ?>" lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>" xmlns:fb="http://www.facebook.com/2008/fbml">

<head>
  <title><?php print $head_title; ?></title>
  <?php print $head; ?>
  <link href='https://fonts.googleapis.com/css?family=Abel' rel='stylesheet' type='text/css'/>
  <?php print $styles; ?>
  <?php print $scripts; ?>
</head>
<body class="<?php print $classes; ?>">

  <div id="page-wrapper"><div id="page">

	<div id="pre-header-wrapper">
		<div id="pre-header">
			<?php print $pre_header; ?>
		</div>
	</div>

    <div id="header"><div class="section clearfix">

		<?php print $header; ?>
		<div class="logo">
			<a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
				<img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
			</a>
		</div>

	</div>	</div><!-- /.section, /#header-inner, /#header -->

	<div id="nav">
		<?php print $nav; ?>
	</div>

	<?php print $messages; ?>
	<?php print $help; ?>
	
	<?php if($highlight) print $highlight; ?>

		<?php if ($home_row_featured_1 || $home_row_featured_2) : ?>
		<div id="home_row_featured" class="clearfix home_row <?php if ($home_row_featured_1 && $home_row_featured_2) {print 'home_row_2_regions';} ?>">
			<?php if ($home_row_featured_1) : ?>
				<div id="home_row_featured_1" class="clearfix home_row_region home_row_first<?php if(!$home_row_featured_2) {print ' home_row_last';} ?>">
					<?php print $home_row_featured_1; ?>
				</div>
			<?php endif; ?>
			<?php if ($home_row_featured_2) : ?>
				<div id="home_row_featured_2" class="clearfix home_row_region <?php if(!$home_row_featured_1) {print 'home_row_first';}?> home_row_last">
					<?php print $home_row_featured_2; ?>
				</div>
			<?php endif; ?>
			<div class="clear">&nbsp;</div>
		</div>
		<?php endif; ?>
	
    <div id="main-wrapper" class="clearfix"><div id="main" class="clearfix">

      <div id="content" class="column"><div class="section">

        <div id="content-area">
        <?php print $content_top; ?>

		 <!-- <div id="mission"><?php print $mission; ?></div> -->
 
		<?php if ($home_row1_1 || $home_row1_2) : ?>
		<div id="home_row1" class="clearfix home_row<?php if ($home_row1_2) {print ' home_row_2_regions';} ?>">
			<div id="home_row1_1" class="home_row_region home_row_first<?php if(!$home_row1_2) {print ' home_row_last';} ?>">
				<?php print $home_row1_1; ?>
			</div>
			<?php if ($home_row1_2) : ?>
			<div id="home_row1_2" class="clearfix home_row_region home_row_last">
				<?php print $home_row1_2; ?>
			</div>
			<?php endif; ?>
		</div>
		<?php endif; ?>

		<?php if ($home_row2_1 || $home_row2_2) : ?>
		<div id="home_row2" class="clearfix home_row<?php if ($home_row2_2) {print ' home_row_2_regions';} ?>">
			<div id="home_row2_1" class="clearfix home_row_region home_row_first<?php if(!$home_row2_2) {print ' home_row_last';} ?>">
				<?php print $home_row2_1; ?>
			</div>
			<?php if ($home_row2_2) : ?>
			<div id="home_row2_2" class="clearfix home_row_region home_row_last">
				<?php print $home_row2_2; ?>
			</div>
			<?php endif; ?>
		</div>
		<?php endif; ?>

		<?php if ($home_row3_1 || $home_row3_2) : ?>
		<div id="home_row3" class="clearfix home_row<?php if ($home_row3_2) {print ' home_row_2_regions';} ?>">
			<div id="home_row3_1" class="clearfix home_row_region home_row_first<?php if(!$home_row3_2) {print ' home_row_last';} ?>">
				<?php print $home_row3_1; ?>
			</div>
			<?php if ($home_row3_2) : ?>
			<div id="home_row3_2" class="clearfix home_row_region home_row_last">
				<?php print $home_row3_2; ?>
			</div>
			<?php endif; ?>
		</div>
		<?php endif; ?>
		
		<?php if ($home_row4_1 || $home_row4_2) : ?>
		<div id="home_row4" class="clearfix home_row<?php if ($home_row4_2) {print ' home_row_2_regions';} ?>">
			<div id="home_row4_1" class="clearfix home_row_region home_row_first<?php if(!$home_row4_2) {print ' home_row_last';} ?>">
				<?php print $home_row4_1; ?>
			</div>
			<?php if ($home_row4_2) : ?>
			<div id="home_row4_2" class="clearfix home_row_region home_row_last">
				<?php print $home_row4_2; ?>
			</div>
			<?php endif; ?>
		</div>
		<?php endif; ?>
		
		<?php if ($home_row5_1 || $home_row5_2) : ?>
		<div id="home_row5" class="clearfix home_row<?php if ($home_row5_2) {print ' home_row_2_regions';} ?>">
			<div id="home_row5_1" class="clearfix home_row_region home_row_first<?php if(!$home_row5_2) {print ' home_row_last';} ?>">
				<?php print $home_row5_1; ?>
			</div>
			<?php if ($home_row5_2) : ?>
			<div id="home_row5_2" class="clearfix home_row_region home_row_last">
				<?php print $home_row5_2; ?>
			</div>
			<?php endif; ?>
		</div>
		<?php endif; ?>
		
		</div>
      </div></div> <!-- /.section, /#content -->

    </div><!-- /#main -->

      <?php print $sidebar_first; ?>

      <?php print $sidebar_second; ?>
	

   <div class="clear">&nbsp;</div>
		</div> <!-- /#main-wrapper -->
		
		<?php if ($home_row_last_1 || $home_row_last_2 || $home_row_last_3) : ?>
		<div id="home_row_last" class="home_row
		<?php if (($home_row_last_1 && $home_row_last_2 && !$home_row_last_3) ||
		($home_row_last_1 && !$home_row_last_2 && $home_row_last_3) ||
		(!$home_row_last_1 && $home_row_last_2 && $home_row_last_3)) print 'home_row_2_regions'; ?>
		<?php if ($home_row_last_1 && $home_row_last_2 && $home_row_last_3) print ' home_row_3_regions'; ?>
		<?php if ($home_row_last_1 && $home_row_last_2 && $home_row_last_3 && $home_row_last_4) print ' home_row_4_regions'; ?>
		<?php if ($home_row_last_1 && $home_row_last_2 && $home_row_last_3 && $home_row_last_4 && $home_row_last_5) print ' home_row_5_regions'; ?>">
		<div class="home_row_last_wrapper">
			<?php if ($home_row_last_1) : ?>
				<div id="home_row_last_1" class="home_row_region home_row_last home_row_first<?php if(!$home_row_last_2 && !$home_row_last_3) {print ' home_row_last';} ?>">
					<?php print $home_row_last_1; ?>
				</div>
			<?php endif; ?>
			<?php if ($home_row_last_2) : ?>
				<div id="home_row_last_2" class="home_row_region home_row_last <?php if(!$home_row_last_1) {print 'home_row_first';}?><?php if($home_row_last_1 && $home_row_last_3) {print ' home_row_middle';} ?><?php if(!$home_row_last_3) {print ' home_row_last';} ?>">
					<?php print $home_row_last_2; ?>
				</div>
			<?php endif; ?>
			<?php if ($home_row_last_3) : ?>
				<div id="home_row_last_3" class="home_row_region home_row_last<?php if(!$home_row_last_1 && !$home_row_last_2) {print ' home_row_first';} ?><?php if(!$home_row_last_4 && !$home_row_last_5) {print ' home_row_last';} ?>">
					<?php print $home_row_last_3; ?>
				</div>
			<?php endif; ?>
			<?php if ($home_row_last_4) : ?>
				<div id="home_row_last_4" class="home_row_region home_row_last <?php if(!$home_row_last_5) {print 'home_row_last';}?>">
					<?php print $home_row_last_4; ?>
				</div>
			<?php endif; ?>
			<?php if ($home_row_last_5) : ?>
				<div id="home_row_last_5" class="home_row_region home_row_last home_row_last">
					<?php print $home_row_last_5; ?>
				</div>
			<?php endif; ?>
			</div>
			<div class="clear">&nbsp;</div>
		</div>
		<?php endif; ?>	
		
      <div id="footer"><div class="section">

        <?php if ($footer_message): ?>
          <div id="footer-message"><?php print $footer_message; ?></div>
        <?php endif; ?>

        <?php print $footer; ?>

      </div></div> <!-- /.section, /#footer -->

  </div></div> <!-- /#page, /#page-wrapper -->

  <?php print $page_closure; ?>

  <?php print $closure; ?>

</body>
</html>
